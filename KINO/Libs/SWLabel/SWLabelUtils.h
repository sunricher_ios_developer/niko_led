//
//  AEUILabelUtils.h
//  PAECSS_iPhone
//
//  Created by Aidian Tang on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Utils)

- (void)sizeToFitFixedWidth:(CGFloat)fixedWidth;
- (void)sizeToFitFixedWidth:(CGFloat)fixedWidth maxNumberOfLines:(NSInteger)mNumOfLines;

@end
