//
//  AEUILabelUtils.m
//  PAECSS_iPhone
//
//  Created by Aidian Tang on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SWLabelUtils.h"

@implementation UILabel (utils)

- (void)sizeToFitFixedWidth:(CGFloat)fixedWidth
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, fixedWidth, 0);
    self.lineBreakMode = NSLineBreakByWordWrapping;
    self.numberOfLines = 0;
    [self sizeToFit];
    NSLog(@"%@", NSStringFromCGRect(self.frame));
}

- (void)sizeToFitFixedWidth:(CGFloat)fixedWidth maxNumberOfLines:(NSInteger)mNumOfLines
{
    CGSize titleSizeForHeight = [self.text sizeWithAttributes:@{NSFontAttributeName:self.font}];
    CGSize titleSizeForLines = [self.text boundingRectWithSize:CGSizeMake(fixedWidth, MAXFLOAT) options:0 attributes:@{NSFontAttributeName:self.font} context:nil].size;
    NSInteger numberOfLines = ceil(titleSizeForLines.height/titleSizeForHeight.height);
    if (numberOfLines > mNumOfLines) {
        numberOfLines = mNumOfLines;
    }
    self.numberOfLines = numberOfLines;
    if (self.numberOfLines <= 1) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, fixedWidth, titleSizeForHeight.height);
    }else {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, fixedWidth, self.numberOfLines*titleSizeForHeight.height);
    }
}

@end
