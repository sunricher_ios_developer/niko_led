//
//  UIViewUtils.h
//  KINO
//
//  Created by Aidian.Tang on 13-12-10.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Utils)

+ (UIImage *)backgroundGradientImageWithSize:(CGSize)size;

@end
