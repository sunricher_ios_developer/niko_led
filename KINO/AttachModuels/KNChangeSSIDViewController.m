//
//  KNChangeSSIDViewController.m
//  KINO
//
//  Created by Aidian.Tang on 14-1-22.
//  Copyright (c) 2014年 Sunricher. All rights reserved.
//

#import "KNChangeSSIDViewController.h"
#import "SWLabelUtils.h"

@interface KNChangeSSIDViewController () <UITextFieldDelegate> {
    KNChangeSSIDViewControllerCompletionBlock _block;
}

@property (nonatomic, strong) IBOutlet UIView *container;
@property (nonatomic, strong) IBOutlet UILabel *desLab;
@property (nonatomic, strong) IBOutlet UIButton *closeBtn;
@property (nonatomic, strong) IBOutlet UIButton *okBtn;
@property (nonatomic, strong) IBOutlet UITextField *ssidTextField;

@end

@implementation KNChangeSSIDViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _container.layer.cornerRadius = 5.;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage *)backgroundGradientImageWithSize:(CGSize)size
{
	CGPoint center = CGPointMake(size.width * 0.5, size.height * 0.5);
	CGFloat innerRadius = 0;
    CGFloat outerRadius = sqrtf(size.width * size.width + size.height * size.height) * 0.5;
    
	BOOL opaque = NO;
    UIGraphicsBeginImageContextWithOptions(size, opaque, [[UIScreen mainScreen] scale]);
	CGContextRef context = UIGraphicsGetCurrentContext();
    
    const size_t locationCount = 2;
    CGFloat locations[locationCount] = { 0.0, 1.0 };
    CGFloat components[locationCount * 4] = {
		0.0, 0.0, 0.0, 0.1, // More transparent black
		0.0, 0.0, 0.0, 0.7  // More opaque black
	};
	
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorspace, components, locations, locationCount);
	
    CGContextDrawRadialGradient(context, gradient, center, innerRadius, center, outerRadius, 0);
	
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGColorSpaceRelease(colorspace);
    CGGradientRelease(gradient);
	
    return image;
}

- (IBAction)close:(id)sender
{
    [self.view.superview removeFromSuperview];
    _block(NO, nil);
}

- (IBAction)ok:(id)sender
{
    if (_ssidTextField.text.length == 0) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:_ssidTextField.placeholder preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
        return;
    }
    
    if (_ssidTextField.text.length > 20) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"Most input 20 characters" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
        return;
    }
    
    NSCharacterSet *disallowedCharacters = [[NSCharacterSet
                                             characterSetWithCharactersInString:@"0123456789QWERTYUIOPLKJHGFDSAZXCVBNMqwertyuioplkjhgfdsazxcvbnm "] invertedSet];
    NSRange foundRange = [_ssidTextField.text rangeOfCharacterFromSet:disallowedCharacters];
    if (foundRange.location != NSNotFound) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"Letters and numbers only" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
        return;
    }
    
    
    
    [self.view.superview removeFromSuperview];
    _block(YES,_ssidTextField.text);
}

- (void)showMessage:(NSString *)messege inView:(UIView *)view withCompletion:(KNChangeSSIDViewControllerCompletionBlock)block
{
    _block = [block copy];
//    CGFloat height = 0;
//    
//    _desLab.text = messege;
//    [_desLab sizeToFitFixedWidth:CGRectGetWidth(_desLab.frame)];
//    height += CGRectGetMaxY(_desLab.frame) + 11;
//    
//    _closeBtn.frame = CGRectMake(_closeBtn.frame.origin.x, height, _closeBtn.frame.size.width, _closeBtn.frame.size.height);
//    _okBtn.frame = CGRectMake(_okBtn.frame.origin.x, height, _okBtn.frame.size.width, _okBtn.frame.size.height);
//    
//    if (_closeHidden) {
//        _closeBtn.hidden = YES;
//        _okBtn.frame = CGRectMake((CGRectGetWidth(_container.frame) - CGRectGetWidth(_okBtn.frame)) / 2., CGRectGetMaxY(_desLab.frame) + 11, _okBtn.frame.size.width, _okBtn.frame.size.height);
//    }
//    height += CGRectGetHeight(_closeBtn.frame) + CGRectGetMinY(_desLab.frame);
//    
//    _container.frame = CGRectMake(_container.frame.origin.x, (CGRectGetHeight(self.view.frame) - height) / 2., _container.frame.size.width, height);
    
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
	UIImageView *dimView = [[UIImageView alloc] initWithFrame:keyWindow.bounds];
	dimView.image = [self backgroundGradientImageWithSize:keyWindow.bounds.size];
	dimView.userInteractionEnabled = YES;
	
	[keyWindow addSubview:dimView];
	[dimView addSubview:self.view];
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.view.superview.alpha = 1;
     }
                     completion:nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
