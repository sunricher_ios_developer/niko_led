//
//  KNDeleteAlertViewController.h
//  KINO
//
//  Created by Aidian.Tang on 13-12-8.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^KNAlertViewControllerCompletionBlock)(BOOL isOk);

@interface KNAlertViewController : UIViewController

@property (nonatomic, assign) BOOL closeHidden;
@property (nonatomic, assign) BOOL  okShowed;

- (void)showMessage:(NSString *)messege inView:(UIView *)view withCompletion:(KNAlertViewControllerCompletionBlock)block;

@end
