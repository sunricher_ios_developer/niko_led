//
//  WPLoadingViewController.m
//  WashGoldParty
//
//  Created by Aidian.Tang on 13-5-24.
//  Copyright (c) 2013年 Winfires. All rights reserved.
//

#import "WFLoadingViewController.h"

@interface WFLoadingViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *bgImgView;

@end

@implementation WFLoadingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *bg = [[UIImageView alloc] initWithFrame:self.view.bounds];
    if ([[UIDevice currentDevice] isDevice5]) {
        bg.image = [UIImage imageNamed:@"splash_xxlarge"];
    } else {
        bg.image = [UIImage imageNamed:@"Splashscreen"];
    }
    if (kSystemVersion < 7) {
        bg.frame = CGRectOffset(bg.frame, 0, 20);
    }
    [self.view addSubview:bg];
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(annimotionDidDone:) userInfo:nil repeats:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)annimotionDidDone:(id)sender
{
    [UIView animateWithDuration:1. animations:^{
        CATransform3D transform = CATransform3DMakeScale(1.5, 1.5, 1.0);
        self.view.layer.transform = transform;
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (_delegate && [_delegate respondsToSelector:@selector(loadingViewControllerDidTimeOver:)]) {
            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
            [_delegate loadingViewControllerDidTimeOver:self];
        };
    }];
}

@end
