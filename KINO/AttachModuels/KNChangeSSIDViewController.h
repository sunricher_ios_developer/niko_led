//
//  KNChangeSSIDViewController.h
//  KINO
//
//  Created by Aidian.Tang on 14-1-22.
//  Copyright (c) 2014年 Sunricher. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^KNChangeSSIDViewControllerCompletionBlock)(BOOL isOk, NSString *SSID);

@interface KNChangeSSIDViewController : UIViewController

@property (nonatomic, assign) BOOL closeHidden;
@property (nonatomic, assign) BOOL  okShowed;

- (void)showMessage:(NSString *)messege inView:(UIView *)view withCompletion:(KNChangeSSIDViewControllerCompletionBlock)block;

@end
