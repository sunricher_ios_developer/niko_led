//
//  KNTutorialViewController.h
//  KINO
//
//  Created by Aidian.Tang on 13-12-7.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KNTutorialViewController;

@protocol KNTutorialViewControllerDelegate <NSObject>

- (void)tutorialViewControllerDidClose:(KNTutorialViewController *)tutorialViewController;

@end

@interface KNTutorialViewController : UIViewController

@property (nonatomic, weak) id<KNTutorialViewControllerDelegate> delegate;

@end
