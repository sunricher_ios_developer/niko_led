//
//  WPLoadingViewController.h
//  WashGoldParty
//
//  Created by Aidian.Tang on 13-5-24.
//  Copyright (c) 2013年 Winfires. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WFLoadingViewController;

@protocol WFLoadingViewControllerDelegate <NSObject>

- (void)loadingViewControllerDidTimeOver:(WFLoadingViewController *)loadingViewController;

@end

@interface WFLoadingViewController : UIViewController

@property (nonatomic, weak) id<WFLoadingViewControllerDelegate> delegate;

@end
