//
//  KNTutorialViewController.m
//  KINO
//
//  Created by Aidian.Tang on 13-12-7.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import "KNTutorialViewController.h"

@interface KNTutorialViewController ()

@property (nonatomic, strong) IBOutlet UIButton *closeBtn;

@end

@implementation KNTutorialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (kSystemVersion < 7.0) {
        _closeBtn.frame = CGRectOffset(_closeBtn.frame, 0, -20);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender
{
    [_delegate tutorialViewControllerDidClose:self];
}

@end
