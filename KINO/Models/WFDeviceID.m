//
//  ECDeviceID.m
//  EasyColor
//
//  Created by Tang Aidian on 12-3-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WFDeviceID.h"

@implementation WFDeviceID

@synthesize firstID = _firstID;
@synthesize middleID = _middleID;
@synthesize lastID = _lastID;

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    
    if (self) {
        self.firstID = [coder decodeIntegerForKey:@"firstID"];
        self.middleID = [coder decodeIntegerForKey:@"middleID"];
        self.lastID = [coder decodeIntegerForKey:@"lastID"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeInteger:_firstID forKey:@"firstID"];
    [coder encodeInteger:_middleID forKey:@"middleID"];
    [coder encodeInteger:_lastID forKey:@"lastID"];
}

@end
