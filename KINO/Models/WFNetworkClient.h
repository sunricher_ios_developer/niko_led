//
//  ECNetworkClient.h
//  EasyColor
//
//  Created by Tang Aidian on 12-3-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WFNetwork.h"

static NSString *kDeviceScanWillStarted = @"kDeviceScanWillStarted";
static NSString *kDeviceScanDidCompleted = @"kDeviceScanDidCompleted";

@class WFDeviceID, WFNetworkClient;

@protocol WFNetworkClientDelegate <NSObject>

@optional
- (void)networkClient:(WFNetworkClient *)networkClient didScanedDeviceIPs:(NSArray *)ips;
- (void)networkClient:(WFNetworkClient *)networkClient didScanedDeviceFailed:(NSError *)error;
- (void)networkClient:(WFNetworkClient *)networkClient didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext;

@end

@interface WFNetworkClient : NSObject

@property (nonatomic, weak) id<WFNetworkClientDelegate> delegate;
//@property (nonatomic,assign) NetworkType networkType;
@property (nonatomic,retain) WFDeviceID *deviceID;
@property (nonatomic, strong) NSMutableArray *devices;
@property (nonatomic,retain) WFNetwork *activeNetwork;
@property (nonatomic,retain) WFNetwork *setupNetwork;//48899
//@property (nonatomic,retain) WFNetwork *wifiNetwork;
//@property (nonatomic,retain) WFNetwork *routerNetwork;
@property (nonatomic,retain) NSArray *netWorks;
@property (nonatomic,retain) NSArray *blueToothNetWorks;
@property (nonatomic, strong) NSMutableArray *rooms;

+ (WFNetworkClient *)sharedNetworkClient;

- (void)sendNetworkData:(WFNetworkData *)networkData;
- (void)archiveNetworks;
- (void)unarchiveNetworks;
- (void)archiveNetwork:(WFNetwork *)network;
- (void)deleteNetWork:(WFNetwork *)network;
- (void)activeNetwork:(WFNetwork *)network;
- (void)inActiveNetwork:(WFNetwork *)network;
- (void)startDeviceScaning;

- (void)activeSetupNetwork;
- (void)inActiveSetupNetwork;

@end
