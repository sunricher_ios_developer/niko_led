//
//  ECNetworkData.m
//  EasyColor
//
//  Created by Tang Aidian on 12-3-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WFNetworkData.h"
#import "WFNetworkClient.h"

#define kCountOfNumbers 5
#define kCountOfSubDeviceNumbers 16

@implementation WFNetworkData

@synthesize firstByte = _firstByte;
@synthesize deviceID = _deviceID;
@synthesize verifyPlus = _verifyPlus;
@synthesize lastByte = _lastByte;

@synthesize numberStatusDic = _numberStatusDic;
@synthesize subDeviceNumberStatusDic = _subDeviceNumberStatusDic;

+ (WFNetworkData *)shareNetworkData
{
    static WFNetworkData *_networkData = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _networkData = [[self alloc] init];
    });
    
    return _networkData;
}

- (id)init {
    self = [super init];
    if (self) {
        self.numberStatusDic = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kNumberStatusDic]];
        if ([[_numberStatusDic allKeys] count] == 0) {
            self.numberStatusDic = [[NSMutableDictionary alloc] init];
            for (int i = 1; i <= kCountOfNumbers; i++) {
                [_numberStatusDic setObject:kInSelectedStatus forKey:[NSString stringWithFormat:@"%d", i]];
            }
        }
        
        self.subDeviceNumberStatusDic = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kSubDeviceNumberStatusDic]];
        if ([[_subDeviceNumberStatusDic allKeys] count] == 0) {
            self.subDeviceNumberStatusDic = [[NSMutableDictionary alloc] init];
            for (int i = 1; i <= kCountOfSubDeviceNumbers; i++) {
                [_subDeviceNumberStatusDic setObject:kInSelectedStatus forKey:[NSString stringWithFormat:@"%d", i]];
            }
        }
    }
    
    return self;
}

- (NSInteger)firstByte
{
    return '\x55';
}

- (WFDeviceID *)deviceID
{
    return [[WFNetworkClient sharedNetworkClient] deviceID];
}

- (NSInteger)verifyPlus
{
    return self.lowSubDeviceNo + self.highSubDeviceNo + self.opcode + self.opvalue1 + self.opvalue2 + self.opvalue3;
}

- (NSInteger)elevenByte
{
    return '\xAA';
}

- (NSInteger)lastByte
{
    return '\xAA';
}

- (NSInteger)lowSubDeviceNo
{
    if (_noAutoCalcuteSubDeviceNo) {
        return _lowSubDeviceNo;
    }
    
    NSInteger result = 0;
    NSArray *keys = [NSArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8",nil];
    
    for (NSString *key in keys) {
        if ([[_subDeviceNumberStatusDic objectForKey:key] isEqualToString:kSelectedStatus]) {
            result += powf(2, [key integerValue] - 1);
        }
    }
    
    return result;
}

- (NSInteger)highSubDeviceNo
{
    if (_noAutoCalcuteSubDeviceNo) {
        return _highSubDeviceNo;
    }
    
    NSInteger result = 0;
    NSArray *keys = [NSArray arrayWithObjects:@"9", @"10", @"11", @"12", @"13", @"14", @"15", @"16",nil];
    
    for (NSString *key in keys) {
        if ([[_subDeviceNumberStatusDic objectForKey:key] isEqualToString:kSelectedStatus]) {
            result += powf(2, [key integerValue] - 1);
        }
    }
    
    return result;
}

- (void)selectNumber:(NSString *)numberStr
{
    [_numberStatusDic setObject:kSelectedStatus forKey:numberStr];
}

- (void)inSelectNumber:(NSString *)numberStr
{
    [_numberStatusDic setObject:kInSelectedStatus forKey:numberStr];
}

- (void)selectAllNumbers
{
    NSArray *allKeys = [NSArray arrayWithArray:[_numberStatusDic allKeys]];
    for (NSString *key in allKeys) {
        NSString *status = [_numberStatusDic objectForKey:key];
        if (![status isEqualToString:kInActivedStatus]) {
            [_numberStatusDic setObject:kSelectedStatus forKey:key];
        }
    }
}

- (void)resetNumberStatus
{
    for (int i = 1; i <= kCountOfNumbers; i++) {
        NSString *key = [NSString stringWithFormat:@"%d", i];
        NSString *status = [_numberStatusDic objectForKey:key];
        if (![status isEqualToString:kInActivedStatus]) {
            [_numberStatusDic setObject:kInSelectedStatus forKey:key];
        }
    }
}

- (void)saveNumberStatus
{
    if (_numberStatusDic) {
        [[NSUserDefaults standardUserDefaults] setObject:_numberStatusDic forKey:kNumberStatusDic];
    }
}

- (void)saveState
{
    [self saveNumberStatus];
    [self saveSubDeviceNumberStatus];
}

- (void)selectSubDeviceNumber:(NSString *)numberStr
{
    [_subDeviceNumberStatusDic setObject:kSelectedStatus forKey:numberStr];
    [self saveSubDeviceNumberStatus];
}

- (void)inSelectSubDeviceNumber:(NSString *)numberStr
{
    [_subDeviceNumberStatusDic setObject:kInSelectedStatus forKey:numberStr];
    [self saveSubDeviceNumberStatus];
}

- (void)selectAllSubDeviceNumbers
{
    NSArray *allKeys = [NSArray arrayWithArray:[_subDeviceNumberStatusDic allKeys]];
    for (NSString *key in allKeys) {
        NSString *status = [_subDeviceNumberStatusDic objectForKey:key];
        if (![status isEqualToString:kInActivedStatus]) {
            [_subDeviceNumberStatusDic setObject:kSelectedStatus forKey:key];
        }
    }
}

- (void)inActiveSubDeviceNumber:(NSString *)numberStr
{
    [_subDeviceNumberStatusDic setObject:kInActivedStatus forKey:numberStr];
}

- (void)resetSubDeviceNumberStatus
{
    for (int i = 1; i <= kCountOfSubDeviceNumbers; i++) {
        NSString *key = [NSString stringWithFormat:@"%d", i];
        NSString *status = [_subDeviceNumberStatusDic objectForKey:key];
        if (![status isEqualToString:kInActivedStatus]) {
            [_subDeviceNumberStatusDic setObject:kInSelectedStatus forKey:key];
        }
    }
    [self saveSubDeviceNumberStatus];
}

- (void)saveSubDeviceNumberStatus
{
    if (_subDeviceNumberStatusDic) {
        [[NSUserDefaults standardUserDefaults] setObject:_subDeviceNumberStatusDic forKey:kSubDeviceNumberStatusDic];
    }
}

@end
