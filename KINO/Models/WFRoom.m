//
//  WFRoom.m
//  RGBW
//
//  Created by Aidian.Tang on 13-10-27.
//  Copyright (c) 2013年 Winfires. All rights reserved.
//

#import "WFRoom.h"


@implementation WFRoom

@dynamic actived;
@dynamic selected;
@dynamic smooth;
@dynamic jump;
@dynamic run;
@dynamic identity;
@dynamic ip;
@dynamic port;
@dynamic mac;
@dynamic name;
@dynamic status;
@dynamic thumbnails;
@dynamic type;

@end
