//
//  ECNetwork.m
//  EasyColor
//
//  Created by Tang Aidian on 12-3-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WFNetwork.h"
#import "GCDAsyncUdpSocket.h"
#import "GCDAsyncSocket.h"
#import "WFNetworkData.h"

#import <ifaddrs.h>
#import <arpa/inet.h>

@interface WFNetwork ()<GCDAsyncSocketDelegate,GCDAsyncUdpSocketDelegate>
{
    NSInteger _tryC;
}

@property (nonatomic, strong) GCDAsyncUdpSocket *asyncUdpSocket;
@property (nonatomic, strong) GCDAsyncSocket *asyncTcpSocket;
@property (nonatomic, strong) NSTimer *timeoutTimer;
@property (nonatomic, assign) NSTimeInterval timeout;
@property (nonatomic, strong) NSData *sendData;

@end

@implementation WFNetwork

@synthesize delegate = _delegate;
@synthesize actived = _actived;
//@synthesize networkType = _networkType;
@synthesize IP = _IP;
@synthesize port = _port;
@synthesize name = _name;
@synthesize SSID = _SSID;
@synthesize UUID = _UUID;

@synthesize connecting = _connecting;

- (void)dealloc
{
    if (_timeoutTimer) {
        [_timeoutTimer invalidate];
    }
}

- (id)copyWithZone:(NSZone *)zone
{
    id copy = [[[self class] alloc] init];
    
    if (copy) {
        // Copy NSObject subclasses
        [copy setIP:[self.IP copyWithZone:zone]];
        [copy setPort:[self.port copyWithZone:zone]];
        [copy setName:[self.name copyWithZone:zone]];
        [copy setSSID:[self.SSID copyWithZone:zone]];
        
        // Set primitives
        //[copy setActive:self.actived];
        
        //[copy setNetworkType:self.networkType];
    }
    
    return copy;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    
    if (self) {
        self.IP = [coder decodeObjectForKey:@"IP"];
        self.port = [coder decodeObjectForKey:@"port"];
        self.name = [coder decodeObjectForKey:@"name"];
        self.SSID = [coder decodeObjectForKey:@"SSID"];
        self.UUID = [coder decodeObjectForKey:@"UUID"];
        //self.actived = [coder decodeBoolForKey:@"actived"];
        //self.networkType = [coder decodeIntegerForKey:@"networkType"];
        self.connectType = [coder decodeIntegerForKey:@"connectType"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:_IP forKey:@"IP"];
    [coder encodeObject:_port forKey:@"port"];
    [coder encodeObject:_name forKey:@"name"];
    [coder encodeObject:_SSID forKey:@"SSID"];
    [coder encodeObject:_UUID forKey:@"UUID"];
    //[coder encodeBool:_actived forKey:@"actived"];
    //[coder encodeInteger:_networkType forKey:@"networkType"];
    [coder encodeInteger:_connectType forKey:@"connectType"];
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)resendNetworkData:(WFNetworkData *)networkData
{
    UInt8 bytes[12];
    bytes[0] = networkData.firstByte;
    bytes[1] = networkData.deviceID.firstID;
    bytes[2] = networkData.deviceID.middleID;
    bytes[3] = networkData.deviceID.lastID;
    bytes[4] = 0;//networkData.highSubDeviceNo;
    bytes[5] = 1;//networkData.lowSubDeviceNo;
    bytes[6] = networkData.opcode;
    bytes[7] = networkData.opvalue1;
    bytes[8] = networkData.opvalue2;
    bytes[9] = networkData.opvalue3;
    bytes[10] = networkData.verifyPlus;
    bytes[11] = networkData.lastByte;
    
    NSMutableString* dString = [NSMutableString stringWithString:@""];
    for(NSInteger i = 0; i < 12; i++)
        [dString appendFormat:@"%d, ", bytes[i]];
    //NSLog(@"sendNetworkData = %@", dString);
    
    if (_connectType == kNetworkConnectTypeUDP) {
        for (NSString * ip in _ips) {
            [_asyncUdpSocket sendData:[NSData dataWithBytes:bytes length:sizeof(bytes)] toHost:ip port:[_port integerValue] withTimeout:-1 tag:0];
        }
    } else {
        [_asyncTcpSocket writeData:[NSData dataWithBytes:bytes length:sizeof(bytes)] withTimeout:-1 tag:0];
    }
}

- (void)sendNetworkData:(WFNetworkData *)networkData
{
    if (self.ipAddressGetFailed) {
    
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Not connected to a WiFi network, connect to the correct WiFi network." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    UInt8 bytes[12];
    bytes[0] = networkData.firstByte;
    bytes[1] = networkData.deviceID.firstID;
    bytes[2] = networkData.deviceID.middleID;
    bytes[3] = networkData.deviceID.lastID;
    bytes[4] = 0;//networkData.highSubDeviceNo;
    bytes[5] = 1;//networkData.lowSubDeviceNo;
    bytes[6] = networkData.opcode;
    bytes[7] = networkData.opvalue1;
    bytes[8] = networkData.opvalue2;
    bytes[9] = networkData.opvalue3;
    bytes[10] = networkData.verifyPlus;
    bytes[11] = networkData.lastByte;
    
    NSMutableString* dString = [NSMutableString stringWithString:@""];
    for(NSInteger i = 0; i < 12; i++)
        [dString appendFormat:@"%d, ", bytes[i]];
    NSLog(@"sendNetworkData = %@", dString);
    
    if (_connectType == kNetworkConnectTypeUDP) {
        for (NSString * ip in _ips) {
            [_asyncUdpSocket sendData:[NSData dataWithBytes:bytes length:sizeof(bytes)] toHost:ip port:[_port integerValue] withTimeout:-1 tag:0];
        }
    } else {
        [_asyncTcpSocket writeData:[NSData dataWithBytes:bytes length:sizeof(bytes)] withTimeout:-1 tag:0];
    }
    [self resendNetworkData:networkData];
}

- (void)sendData:(NSData *)data
{
    if (_connectType == kNetworkConnectTypeUDP) {
       [_asyncUdpSocket sendData:data toHost:_IP port:[_port integerValue] withTimeout:-1 tag:0];
    } else {
        [_asyncTcpSocket writeData:data withTimeout:-1 tag:0];
    }
}

- (void)sendData:(NSData *)data withTimeout:(NSTimeInterval)timeout tryCount:(NSInteger)tryCount
{
    self.sendData = data;
    self.timeout = timeout;
    self.tryCount = tryCount;
    _tryC = _tryCount;
    //    if (_tryCount > 0) {
    //        timeout = timeout / _tryCount;
    //    }
    if (timeout > 0) {
        self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(timeout:) userInfo:nil repeats:YES];
    }
    
    if (_connectType == kNetworkConnectTypeUDP) {
        [_asyncUdpSocket sendData:data toHost:_IP port:[_port integerValue] withTimeout:-1 tag:0];
    } else {
        [_asyncTcpSocket writeData:data withTimeout:-1 tag:0];
    }
}

// Get IP Address
- (NSString *)getIPAddress {
    NSString *address = nil;
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

- (void)refreshIP
{
    NSString *ip = [self getIPAddress];
    if (!ip || ip.length == 0) {
        self.ipAddressGetFailed = YES;
    } else {
        self.ipAddressGetFailed = NO;
    }
    
    NSMutableArray *duans = [NSMutableArray arrayWithArray:[ip componentsSeparatedByString:@"."]];
    NSString *newIp = [NSString stringWithFormat:@""];
    if (duans && duans.count > 0) {
        for (NSInteger i = 0; i < duans.count - 1; i++) {
            NSString *duan = duans[i];
            newIp = [newIp stringByAppendingFormat:@"%@.", duan];
        }
        NSString *newIpPre = [newIp copy];
        if ([newIpPre isEqualToString:@"10.10.100."]) {
            newIp = [newIp stringByAppendingString:@"254"];
            self.connectType = kNetworkConnectTypeTCP;
        } else {
            newIp = [newIp stringByAppendingString:@"255"];
            self.connectType = kNetworkConnectTypeUDP;
        }
    }
    NSLog(@"refreshIP = %@, newIp = %@", ip, newIp);
    
    self.IP = newIp;
}

- (NSString *)description
{
    NSString *str = [NSString stringWithFormat:@"%@,%@,%@,%@", _IP, _name, _port, _SSID];
    NSLog(@"%@", str);
    
    return str;
}

- (void)timeout:(NSTimer *)timer
{
    if (_tryC > 0 ) {
        _tryC--;
        [_asyncUdpSocket sendData:_sendData toHost:_IP port:[_port integerValue] withTimeout:-1 tag:0];
    } else if (_delegate && [_delegate respondsToSelector:@selector(networkDidTimeout:)]) {
        [timer invalidate];
        [_delegate networkDidTimeout:self];
    }
}

- (void)initTCP
{
    if (_asyncTcpSocket) {
        [_asyncTcpSocket disconnect];
        _asyncTcpSocket.delegate = nil;
    }
    
    self.asyncTcpSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    NSError *error = nil;
    if (![_asyncTcpSocket connectToHost:_IP onPort:[_port integerValue] withTimeout:10 error:&error]) {
		self.actived = NO;
	}
}

- (void)initUDP
{
    if (_asyncUdpSocket) {
        [_asyncUdpSocket close];
    }
    
    self.asyncUdpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    NSError *error = nil;
    [_asyncUdpSocket enableBroadcast:YES error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
    if (![_asyncUdpSocket bindToPort:0 error:&error])
    {
        self.actived = NO;
    }
    if (![_asyncUdpSocket beginReceiving:&error])
    {
        self.actived = NO;
    }
    
    self.actived = YES;
}

- (void)active
{
    if (_connectType == kNetworkConnectTypeUDP) {
        [self initUDP];
    } else {
        [self initTCP];
    }
}

- (void)activeWithPort
{
    if (_asyncUdpSocket) {
        [_asyncUdpSocket close];
    }
    
    self.asyncUdpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    NSError *error = nil;
    
    if (error) {
        NSLog(@"%@", error);
    }
    
    [_asyncUdpSocket bindToPort:[_port intValue] error:&error];
    
    if (![_asyncUdpSocket beginReceiving:&error])
    {
        self.actived = NO;
    }
    
    [_asyncUdpSocket enableBroadcast:YES error:&error];
    
    self.actived = YES;
}

- (void)inActive
{
    if (_connectType == kNetworkConnectTypeUDP) {
        if (_asyncUdpSocket) {
            [_asyncUdpSocket close];
        }
    } else {
        if (_asyncTcpSocket && _asyncTcpSocket.isConnected) {
            [_asyncTcpSocket disconnect];
        }
    }
    
    self.actived = NO;
}

- (void)clearAllDatas
{
    self.actived = NO;
    self.IP = nil;
    self.port = nil;
    self.name = nil;
    self.SSID = nil;
}

#pragma mark - TCP Delegate

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
    self.actived = YES;
}

- (void)socketDidSecure:(GCDAsyncSocket *)sock
{
	// This method will be called if USE_SECURE_CONNECTION is set
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{

}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{

}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
	// Since we requested HTTP/1.0, we expect the server to close the connection as soon as it has sent the response.
	self.actived = NO;
}

#pragma mark - UDP Delegate

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
	// You could add checks here
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
	// You could add checks here
    NSLog(@"%@", error);
    [_timeoutTimer invalidate];
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
	NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@", msg);
    [_timeoutTimer invalidate];
    if (_delegate && [_delegate respondsToSelector:@selector(network:didReceiveData:fromAddress:withFilterContext:)]) {
        [_delegate network:self didReceiveData:data fromAddress:address withFilterContext:filterContext];
    }
}

@end
