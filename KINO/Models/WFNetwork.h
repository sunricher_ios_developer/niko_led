//
//  ECNetwork.h
//  EasyColor
//
//  Created by Tang Aidian on 12-3-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

//typedef enum {
//    kNetworkTypeDefault,
//    kNetworkTypeRouter,
//    kNetworkTypeWifi,
//} NetworkType;

typedef enum {
    kNetworkConnectTypeDefault,
    kNetworkConnectTypeTCP,
    kNetworkConnectTypeUDP,
} NetworkConnectType;

@class GCDAsyncUdpSocket, WFNetworkData, WFNetwork;

@protocol WFNetworkDelegate <NSObject>

@optional
- (void)networkDidConnect:(WFNetwork *)network;
- (void)networkDidDisConnect:(WFNetwork *)network;
- (void)network:(WFNetwork *)network didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext;
- (void)networkDidTimeout:(WFNetwork *)network;

@end

@interface WFNetwork : NSObject <NSCoding, NSCopying> {
    
}

@property (nonatomic,weak) id<WFNetworkDelegate>    delegate;
@property (nonatomic,assign) BOOL                   actived; // 是否处于激活状态
@property (nonatomic,assign) BOOL                   ipAddressGetFailed;
@property (nonatomic,assign) NSInteger              tryCount;
//@property (nonatomic,assign) NetworkType            networkType; // 网络类型
@property (nonatomic,assign) NetworkConnectType     connectType; // 连接类型 默认TCP
@property (nonatomic,copy) NSString*                IP; // IP地址
@property (nonatomic,copy) NSString*                port; // 网络端口
@property (nonatomic,copy) NSString*                name; // 网络名称
@property (nonatomic,copy) NSString*                SSID; // wifi网络名称
@property (nonatomic,copy) NSString*                UUID; // 蓝牙设备唯一标

@property (nonatomic,assign) BOOL connecting;

@property (nonatomic, copy)  NSArray *ips;

- (void)sendNetworkData:(WFNetworkData *)networkData;
- (void)sendData:(NSData *)data;
- (void)sendData:(NSData *)data withTimeout:(NSTimeInterval)timeout tryCount:(NSInteger)tryCount;
- (void)active;
- (void)activeWithPort;
- (void)inActive;
- (void)clearAllDatas;
- (void)refreshIP;

@end
