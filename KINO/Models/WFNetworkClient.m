//
//  ECNetworkClient.m
//  EasyColor
//
//  Created by Tang Aidian on 12-3-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WFNetworkClient.h"
#import "WFDeviceID.h"
#import "WFNetworkData.h"
#import "KNAppDelegate.h"
#import "WFRoom.h"

#define kNectworksFileName  @"WFNetworks"
#define kDeviceIDFileName   @"WFDeviceID"
#define kNetworkType        @"kNetworkType"

@interface WFNetworkClient () <WFNetworkDelegate> {
    BOOL _deviceScaning;
}

@end

@implementation WFNetworkClient

//@synthesize networkType = _networkType;
@synthesize netWorks = _netWorks;
@synthesize deviceID = _deviceID;
//@synthesize activeNetwork = _activeNetwork;
@synthesize blueToothNetWorks = _blueToothNetWorks;

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (WFNetworkClient *)sharedNetworkClient
// See comment in header.
{
    static WFNetworkClient * sNetworkClient;
    
    // This can be called on any thread, so we synchronise.  We only do this in
    // the sNetworkManager case because, once sNetworkManager goes non-nil, it can
    // never go nil again.
    
    if (sNetworkClient == nil) {
        @synchronized (self) {
            sNetworkClient = [[WFNetworkClient alloc] init];
            assert(sNetworkClient != nil);
        }
    }
    return sNetworkClient;
}

- (void)unarchiveNetworks
{
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *savedArrayData = [currentDefaults objectForKey:kNectworksFileName];
    if (savedArrayData != nil)
    {
        self.netWorks = [NSKeyedUnarchiver unarchiveObjectWithData:savedArrayData];
        //        for (WFNetwork *network in _netWorks) {
        //            if (network.networkType == kNetworkTypeRouter) {
        //                self.routerNetwork = network;
        //            } else if (network.networkType == kNetworkTypeWifi) {
        //                self.wifiNetwork = network;
        //            }
        //        }
        if (_netWorks.count > 0) {
            self.activeNetwork = [_netWorks objectAtIndex:0];
        }
    }
}

- (void)archiveDeviceID
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:_deviceID] forKey:kDeviceIDFileName];
}

- (void)unarchiveDeviceID
{
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *savedArrayData = [currentDefaults objectForKey:kDeviceIDFileName];
    if (savedArrayData != nil)
    {
        self.deviceID = [NSKeyedUnarchiver unarchiveObjectWithData:savedArrayData];
    }
}

//- (void)setNetworkType:(NetworkType)networkType
//{
//    _networkType = networkType;
//    [[NSUserDefaults standardUserDefaults] setInteger:_networkType forKey:kNetworkType];
//}

- (id)init {
    self = [super init];
    if (self) {
        [self unarchiveNetworks];
        self.rooms = [NSMutableArray array];
    }
    return self;
}

//- (WFNetwork *)activeNetwork
//{
//    if (!_activeNetwork && [_netWorks count] > 0) {
//        self.activeNetwork = [_netWorks objectAtIndex:0];
//    }
//    return _activeNetwork;
//}

- (WFDeviceID *)deviceID
{
    if (!_deviceID) {
        [self unarchiveDeviceID];
        if (!_deviceID) {
            WFDeviceID *deviceID = [[WFDeviceID alloc] init];
            deviceID.firstID = 153;//(arc4random() % 255) + 1;
            deviceID.middleID = (arc4random() % 255) + 1;
            deviceID.lastID = (arc4random() % 255) + 1;
            self.deviceID = deviceID;
            [self archiveDeviceID];
        }
    }
    return _deviceID;
}

- (void)sendNetworkData:(WFNetworkData *)networkData
{
    NSMutableArray *ips = [NSMutableArray array];
    for (WFRoom *room in _rooms) {
        if (room.selected.boolValue) {
            [ips addObject:room.ip];
        }
    }
    self.activeNetwork.ips = ips;
    [self.activeNetwork sendNetworkData:networkData];
}

- (void)archiveNetworks
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:_netWorks] forKey:kNectworksFileName];
}

- (void)archiveNetwork:(WFNetwork *)network
{
    if (network) {
        NSMutableArray *networks = [NSMutableArray arrayWithArray:_netWorks];
        [networks addObject:network];
        self.netWorks = networks;
        [self archiveNetworks];
    }
}

- (void)deleteNetWork:(WFNetwork *)network
{
    if (network) {
        NSMutableArray *networks = [NSMutableArray arrayWithArray:_netWorks];
        [network inActive];
        [networks removeObject:network];
        self.netWorks = networks;
        [self archiveNetworks];
    }
}

- (void)activeNetwork:(WFNetwork *)network
{
    if (network && !network.actived) {
        [network active];
    }
}

- (void)inActiveNetwork:(WFNetwork *)network
{
    if (network && network.actived) {
        [network inActive];
    }
}

- (void)startDeviceScaning
{
    _deviceScaning = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:kDeviceScanWillStarted object:nil];
    
    _activeNetwork.ips = nil;
    [_setupNetwork refreshIP];
    if (_setupNetwork.connectType == kNetworkConnectTypeTCP) {
        _setupNetwork.IP = @"10.10.100.255";
        _setupNetwork.connectType = kNetworkConnectTypeUDP;
    }
    
    if (!_setupNetwork.ipAddressGetFailed) {
        self.devices = [NSMutableArray array];
        
        for (int j=0;j<30;j++)
        {
            NSData *data = [@"HF-A11ASSISTHREAD" dataUsingEncoding:NSASCIIStringEncoding];
            [_setupNetwork sendData:data withTimeout:10 tryCount:3];
        }
        [NSTimer scheduledTimerWithTimeInterval:3. target:self selector:@selector(scanTimerOver:) userInfo:nil repeats:NO];
    } else {
        if (_delegate && [_delegate respondsToSelector:@selector(networkClient:didScanedDeviceFailed:)]) {
            [_delegate networkClient:self didScanedDeviceFailed:[NSError errorWithDomain:@"ScanDevice" code:0 userInfo:[NSDictionary dictionaryWithObject:@"Not connected to the network, please reconnect." forKey:NSLocalizedDescriptionKey]]];
        }
    }
}

- (KNAppDelegate *)appDelegate
{
    return (KNAppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (NSManagedObjectContext *)managedObjectContext
{
    KNAppDelegate *delegate = self.appDelegate;
    return delegate.managedObjectContext;
}

- (NSInteger)roomCount
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"WFRoom" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *rooms = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return rooms.count;
}

- (void)scanComplete
{
    if (_devices.count > 0) {
        NSInteger roomCount = self.roomCount;
        [self.rooms removeAllObjects];
        for (NSString *dev in _devices) {
            NSArray *devInfos = [dev componentsSeparatedByString:@","];
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"WFRoom" inManagedObjectContext:self.managedObjectContext];
            [fetchRequest setEntity:entity];
            NSPredicate *pre = [NSPredicate predicateWithFormat:@"%K = %@", @"mac", [devInfos objectAtIndex:1]];
            [fetchRequest setPredicate:pre];
            
            NSError *error = nil;
            NSArray *rooms = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            WFRoom *roomDev = nil;
            if ([rooms count] == 0) {
                WFRoom *room = [NSEntityDescription insertNewObjectForEntityForName:@"WFRoom" inManagedObjectContext:self.managedObjectContext];
                room.mac = [devInfos objectAtIndex:1];
                room.identity = [NSString stringWithFormat:@"%d", roomCount + 1];
                room.smooth = [NSNumber numberWithBool:YES];
                room.actived = [NSNumber numberWithBool:YES];
                //room.selected = [NSNumber numberWithBool:YES];
                room.type = [NSNumber numberWithInt:kWFRoomTypeRGB];
                room.thumbnails = UIImagePNGRepresentation([UIImage imageNamed:@"img_room_default"]);
                room.name = [NSString stringWithFormat:@"Zone%d", roomCount + 1];
                roomCount++;
                roomDev = room;
            } else {
                roomDev = [rooms objectAtIndex:0];
            }
            
            roomDev.ip = [devInfos objectAtIndex:0];
            [self.appDelegate saveContext];
            [_rooms addObject:roomDev];

            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"identity" ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray = [_rooms sortedArrayUsingDescriptors:sortDescriptors];
            self.rooms = [NSMutableArray arrayWithArray:sortedArray];
        }
        
        if (_delegate && [_delegate respondsToSelector:@selector(networkClient:didScanedDeviceIPs:)]) {
            [_delegate networkClient:self didScanedDeviceIPs:_devices];
        }
    } else {
        if (_delegate && [_delegate respondsToSelector:@selector(networkClient:didScanedDeviceFailed:)]) {
            [_delegate networkClient:self didScanedDeviceFailed:[NSError errorWithDomain:@"ScanDevice" code:0 userInfo:[NSDictionary dictionaryWithObject:@"No RGB(W) controller found. Connect to the correct WiFi network." forKey:NSLocalizedDescriptionKey]]];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDeviceScanDidCompleted object:nil];
    _deviceScaning = NO;
}

- (void)scanTimerOver:(id)sender
{
    [self performSelectorOnMainThread:@selector(scanComplete) withObject:nil waitUntilDone:NO];
}

#pragma mark - WFNetworkDelegate

- (void)network:(WFNetwork *)network didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext
{
    NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"NetWorkClient %@", msg);
    
    if (!_deviceScaning) {
        if (_delegate && [_delegate respondsToSelector:@selector(networkClient:didReceiveData:fromAddress:withFilterContext:)]) {
            [_delegate networkClient:self didReceiveData:data fromAddress:address withFilterContext:filterContext];
        }
        return;
    }
    
    NSString *regEx = @"^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(.?)){4},[0-9A-Z]{12},[0-9A-Z]*";
    NSRange myRange = [msg rangeOfString:regEx options:NSRegularExpressionSearch];
    if (myRange.location == NSNotFound){
        NSLog(@"normal msg: %@", msg);
        //send notification to 2nd page
    }
    // the string is valid
    else
    {
        BOOL hasExited = NO;
        for (NSString *dev in _devices) {
            if ([dev isEqualToString:msg]) {
                hasExited = YES;
                break;
            }
        }
        if (!hasExited) {
            [_devices addObject:msg];
        }
    }
}

- (void)activeSetupNetwork
{
    if (!_setupNetwork) {
        WFNetwork *netWork = [[WFNetwork alloc] init];
        netWork.delegate = self;
        netWork.port = @"48899";
        netWork.connectType = kNetworkConnectTypeUDP;
        [netWork activeWithPort];
        self.setupNetwork = netWork;
    }
}

- (void)inActiveSetupNetwork
{
    _setupNetwork.delegate = nil;
    [_setupNetwork inActive];
    self.setupNetwork = nil;
}

@end
