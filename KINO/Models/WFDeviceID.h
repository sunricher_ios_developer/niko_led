//
//  ECDeviceID.h
//  EasyColor
//
//  Created by Tang Aidian on 12-3-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WFDeviceID : NSObject <NSCoding>

@property (nonatomic,assign) NSInteger firstID;
@property (nonatomic,assign) NSInteger middleID;
@property (nonatomic,assign) NSInteger lastID;

@end
