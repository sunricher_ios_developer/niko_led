//
//  WFRoom.h
//  RGBW
//
//  Created by Aidian.Tang on 13-10-27.
//  Copyright (c) 2013年 Winfires. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef enum {
    kWFRoomTypeDefault              = 0,
    kWFRoomTypeRGB                  = 1 << 0,
    kWFRoomTypeRGBW                 = 1 << 1,
    kWFRoomTypeCWWW                 = 1 << 2,
}WFRoomType;

@interface WFRoom : NSManagedObject

@property (nonatomic, retain) NSNumber * actived;
@property (nonatomic, retain) NSNumber * selected;
@property (nonatomic, retain) NSNumber * smooth;
@property (nonatomic, retain) NSNumber * jump;
@property (nonatomic, retain) NSNumber * run;
@property (nonatomic, retain) NSString * identity;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * ip;
@property (nonatomic, retain) NSString * port;
@property (nonatomic, retain) NSString * mac;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSData * thumbnails;
@property (nonatomic, retain) NSNumber * type;

@end
