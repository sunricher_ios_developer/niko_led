//
//  ECNetworkData.h
//  EasyColor
//
//  Created by Tang Aidian on 12-3-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WFDeviceID.h"

static NSString *kNumberStatusDic =             @"kNumberStatusDic";
static NSString *kSubDeviceNumberStatusDic =    @"kSubDeviceNumberStatusDic";

static NSString *kSelectedStatus =              @"kSelectedStatus";
static NSString *kInSelectedStatus =            @"kInSelectedStatus";
static NSString *kInActivedStatus =             @"kInActivedStatus";

typedef enum {
    kDataTypeDisk = 1,
    kDataTypeBtn = 2,
    kDataTypeVoice = 3,
    kDataTypeGesture = 4,
    kDataTypeSlider = 8,
}DataType;

@interface WFNetworkData : NSObject

@property (nonatomic,assign) NSInteger firstByte;       //55
@property (nonatomic,retain) WFDeviceID *deviceID;      //设备生成3位ID
@property (nonatomic,assign) NSInteger lowSubDeviceNo;  //子机选择位
@property (nonatomic,assign) NSInteger highSubDeviceNo; //子机选择位
@property (nonatomic,assign) NSInteger opcode;          //操作码
@property (nonatomic,assign) NSInteger opvalue1;        //操作值1
@property (nonatomic,assign) NSInteger opvalue2;        //操作值2
@property (nonatomic,assign) NSInteger opvalue3;        //操作值3
@property (nonatomic,assign) NSInteger verifyPlus;      //前六位校验和
@property (nonatomic,assign) NSInteger lastByte;        //AA

@property (nonatomic,retain) NSMutableDictionary *numberStatusDic;
@property (nonatomic,retain) NSMutableDictionary *subDeviceNumberStatusDic;

@property (nonatomic,assign) BOOL noAutoCalcuteSubDeviceNo; //用这个就别用单例

+ (WFNetworkData *)shareNetworkData;

// 从1开始
- (void)selectSubDeviceNumber:(NSString *)numberStr;
- (void)inSelectSubDeviceNumber:(NSString *)numberStr;
- (void)selectAllSubDeviceNumbers;
- (void)inActiveSubDeviceNumber:(NSString *)numberStr;
- (void)resetSubDeviceNumberStatus;
- (void)saveSubDeviceNumberStatus;

- (void)selectNumber:(NSString *)numberStr;
- (void)inSelectNumber:(NSString *)numberStr;
- (void)selectAllNumbers;
- (void)resetNumberStatus;
- (void)saveNumberStatus;

- (void)saveState;

@end
