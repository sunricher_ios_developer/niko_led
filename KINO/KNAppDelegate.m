//
//  KNAppDelegate.m
//  KINO
//
//  Created by Aidian.Tang on 13-12-7.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import "KNAppDelegate.h"
#import "WFRoom.h"
#import "WFNetworkClient.h"

@interface KNAppDelegate () <WFNetworkClientDelegate> {
    BOOL _scanDeviceErrorShowed;
    BOOL wifiAlertShow;
}

@end

@implementation KNAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"WFRoom" inManagedObjectContext:self.managedObjectContext];
//    [fetchRequest setEntity:entity];
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"identity" ascending:YES];
//    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
//    [fetchRequest setSortDescriptors:sortDescriptors];
//    
//    NSError *error = nil;
//    NSArray *rooms = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
//    if ([rooms count] == 0) {
//        WFRoom *room = [NSEntityDescription insertNewObjectForEntityForName:@"WFRoom" inManagedObjectContext:self.managedObjectContext];
//        room.identity = @"1";
//        room.smooth = [NSNumber numberWithBool:YES];
//        room.actived = [NSNumber numberWithBool:YES];
//        room.selected = [NSNumber numberWithBool:YES];
//        room.type = [NSNumber numberWithInt:kWFRoomTypeRGB];
//        room.thumbnails = UIImagePNGRepresentation([UIImage imageNamed:@"img_room_default"]);
//        room.name = @"Room1";
//        NSLog(@"添加房间 %@", room.identity);
//    }
//    [self saveContext];
    
    // 随机生成3位ID
    [[WFNetworkClient sharedNetworkClient] deviceID];
    
    //
    //
    WFNetworkClient *networkClient = [WFNetworkClient sharedNetworkClient];
    [networkClient unarchiveNetworks];
    
    if (networkClient.netWorks.count == 0) {
        WFNetwork *netWork = [WFNetwork alloc];
        netWork.connectType = kNetworkConnectTypeUDP;
        netWork.SSID = @"KINO";
        [netWork refreshIP];
        netWork.port = @"8899";
        netWork.name = @"KINO";
        [networkClient archiveNetwork:netWork];
        networkClient.activeNetwork = netWork;
    }
    
    //[UIApplication sharedApplication].idleTimerDisabled = YES;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self saveContext];
    WFNetworkClient *networkClient = [WFNetworkClient sharedNetworkClient];
    [networkClient inActiveSetupNetwork];
    [networkClient.activeNetwork inActive];
    
    for (WFRoom *room in [WFNetworkClient sharedNetworkClient].rooms) {
        room.selected = [NSNumber numberWithBool:NO];
    }
    [self saveContext];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    WFNetworkClient *networkClient = [WFNetworkClient sharedNetworkClient];
    [networkClient.activeNetwork refreshIP];
    if (networkClient.activeNetwork.ipAddressGetFailed) {
        if(!wifiAlertShow){
            wifiAlertShow = YES;
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"Not connected to a WiFi network, connect to the correct WiFi network." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                wifiAlertShow = NO;
            }];
            [alertVC addAction:okAction];
            [self.window.rootViewController presentViewController:alertVC animated:YES completion:nil];

        }
        
    } else {
        [[WFNetworkClient sharedNetworkClient] archiveNetwork:networkClient.activeNetwork];
        [WFNetworkClient sharedNetworkClient].delegate = self;
        [MBProgressHUD showHUDAddedTo:self.window.rootViewController.view animated:YES];
        [[WFNetworkClient sharedNetworkClient] activeSetupNetwork];
        [[WFNetworkClient sharedNetworkClient] startDeviceScaning];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Models" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Models.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - WFNetworkClientDelegate

- (void)networkClient:(WFNetworkClient *)networkClient didScanedDeviceIPs:(NSArray *)ips
{
    [networkClient.activeNetwork active];
    [MBProgressHUD hideAllHUDsForView:self.window.rootViewController.view animated:YES];
}

- (void)networkClient:(WFNetworkClient *)networkClient didScanedDeviceFailed:(NSError *)error
{
    if (!_scanDeviceErrorShowed) {
        _scanDeviceErrorShowed = YES;
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _scanDeviceErrorShowed = NO;
        }];
        [alertVC addAction:okAction];
        [self.window.rootViewController presentViewController:alertVC animated:YES completion:nil];
    }
    [MBProgressHUD hideAllHUDsForView:self.window.rootViewController.view animated:YES];
}

@end
