//
//  KNEditRoomViewController.m
//  KINO
//
//  Created by Aidian.Tang on 13-12-8.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import "KNEditRoomViewController.h"
#import "KNAlertViewController.h"
#import "KNAddRoomViewController.h"
#import "KNAppDelegate.h"

@interface KNEditRoomViewController () <UITextFieldDelegate, KNAddRoomViewControllerDelegate> {
    WFRoomType _roomType;
}

@property (nonatomic, strong) IBOutlet UIButton *deleteBtn;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UIView *container;
@property (nonatomic, strong) IBOutlet UITextField *nameTf;

@property (nonatomic, strong) IBOutlet UIButton *chooseRGBBtn;
@property (nonatomic, strong) IBOutlet UIButton *chooseRGBWBtn;
@property (nonatomic, strong) IBOutlet UIButton *chooseCWWWBtn;

@property (nonatomic, strong) KNAlertViewController *deleteAlertViewController;

@end

@implementation KNEditRoomViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [_imageView setImage:_image];
    if ([[UIDevice currentDevice] isDevice5]) {
        _container.frame = CGRectMake(0, (CGRectGetHeight(self.view.frame) - CGRectGetHeight(_container.frame)) / 2., CGRectGetWidth(_container.frame), CGRectGetHeight(_container.frame));
    }
    
    if (_room) {
        _imageView.image = [UIImage imageWithData:_room.thumbnails];
        _nameTf.text = _room.name;
        switch (_room.type.integerValue) {
            case kWFRoomTypeRGB:
                _roomType = kWFRoomTypeRGB;
                _chooseRGBBtn.selected = YES;
                break;
            case kWFRoomTypeRGBW:
                _roomType = kWFRoomTypeRGBW;
                _chooseRGBWBtn.selected = YES;
                break;
            case kWFRoomTypeCWWW:
                _roomType = kWFRoomTypeCWWW;
                _chooseCWWWBtn.selected = YES;
                break;
            default:
                break;
        }
    } else {
        self.deleteBtn.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setter

- (void)setImage:(UIImage *)image
{
    _image = image;
    [_imageView setImage:image];
}

#pragma mark - Private

- (KNAppDelegate *)appDelegate
{
    return (KNAppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (NSManagedObjectContext *)managedObjectContext
{
    KNAppDelegate *delegate = self.appDelegate;
    return delegate.managedObjectContext;
}

- (int)getIndexOfRoomNotExited
{
    int index = 1;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"WFRoom" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"identity" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSError *error = nil;
    NSArray *rooms = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    for (WFRoom *room in rooms) {
        if (room.identity.integerValue != index) {
            index = room.identity.intValue;
            break;
        } else {
            index++;
        }
    }
    
    return index;
}

- (void)sendData:(WFNetworkData *)networkData
{
    WFNetworkClient *networkClient = [WFNetworkClient sharedNetworkClient];
    [networkClient sendNetworkData:networkData];
}

#pragma mark - Action

- (IBAction)cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    if (_delegate && [_delegate respondsToSelector:@selector(editRoomViewControllerDidCancel:)]) {
        [_delegate editRoomViewControllerDidCancel:self];
    }
}

- (IBAction)save:(id)sender
{
    if (_nameTf.text.length == 0) {
        [MBProgressHUD showError:@"Please input name" toView:nil];
        return;
    }
    
    if (_roomType == kWFRoomTypeDefault) {
        [MBProgressHUD showError:@"Please select type" toView:nil];
        return;
    }
    
    if (!_room) {
        int indexOfRoomNotExited = [self getIndexOfRoomNotExited];
        WFRoom *room = [NSEntityDescription insertNewObjectForEntityForName:@"WFRoom" inManagedObjectContext:self.managedObjectContext];
        room.identity = [NSString stringWithFormat:@"%d", indexOfRoomNotExited];
        room.smooth = [NSNumber numberWithBool:YES];
        room.actived = [NSNumber numberWithBool:YES];
        room.type = [NSNumber numberWithInt:_roomType];
        if (_image) {
            room.thumbnails = UIImagePNGRepresentation(_image);
        }
        room.name = _nameTf.text;
        NSLog(@"添加房间 %@", room.identity);
    } else {
        _room.type = [NSNumber numberWithInt:_roomType];
        if (_image) {
            _room.thumbnails = UIImagePNGRepresentation(_image);
        }
        _room.name = _nameTf.text;
    }
    [self.appDelegate saveContext];
    
    
    if (_delegate && [_delegate respondsToSelector:@selector(editRoomViewControllerDidSave:)]) {
        [_delegate editRoomViewControllerDidSave:self];
    }
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotiRoomDidSaved object:nil];
}

- (IBAction)delete:(id)sender
{
//    KNAlertViewController *deleteAlertViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"KNAlertViewController"];
//    deleteAlertViewController.view.frame = self.view.bounds;
//    [deleteAlertViewController showMessage:@"Are you sure you want to delete this room?" inView:self.view withCompletion:^(BOOL isOk) {
//        if (isOk) {
//            [self.managedObjectContext deleteObject:_room];
//            [self.appDelegate saveContext];
//            [self.navigationController popViewControllerAnimated:YES];
//            [[NSNotificationCenter defaultCenter] postNotificationName:kNotiRoomDidDeleted object:nil];
//            
//            WFNetworkData *networkData = [WFNetworkData shareNetworkData];
//            networkData.opcode = '\xF1';
//            networkData.opvalue1 = '\x00';
//            networkData.opvalue2 = '\x00';
//            networkData.opvalue3 = '\x00';
//            [self sendData:networkData];
//        }
//    }];
//    self.deleteAlertViewController = deleteAlertViewController;
}

- (IBAction)roomType:(UIButton *)btn
{
    _chooseRGBBtn.selected = NO;
    _chooseRGBWBtn.selected = NO;
    _chooseCWWWBtn.selected = NO;
    
    switch (btn.tag) {
        case 0:
            _roomType = kWFRoomTypeRGB;
            _chooseRGBBtn.selected = YES;
            break;
        case 1:
            _roomType = kWFRoomTypeRGBW;
            _chooseRGBWBtn.selected = YES;
            break;
        case 2:
            _roomType = kWFRoomTypeCWWW;
            _chooseCWWWBtn.selected = YES;
            break;
        default:
            break;
    }
}

- (IBAction)longTap:(UILongPressGestureRecognizer *)tgr
{
    if (tgr.state == UIGestureRecognizerStateBegan) {
        [self performSegueWithIdentifier:@"toKNAddRoomViewController" sender:nil];
    }
}

- (IBAction)doubleTap:(UITapGestureRecognizer *)tgr
{
    if (tgr.state == UIGestureRecognizerStateEnded) {
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        networkData.opcode = '\xF0';
        networkData.opvalue1 = '\x01';
        networkData.opvalue2 = '\x00';
        networkData.opvalue3 = '\x00';
        [self sendData:networkData];
    }
}

- (IBAction)roomImageDidPressed:(UIButton *)btn
{
    WFNetworkData *networkData = [WFNetworkData shareNetworkData];
    networkData.opcode = '\xF0';
    networkData.opvalue1 = '\x02';
    networkData.opvalue2 = '\x00';
    networkData.opvalue3 = '\x00';
    [self sendData:networkData];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //看答案
    if ([segue.identifier isEqualToString:@"toKNAddRoomViewController"]) {
        KNAddRoomViewController *addRoomViewController = segue.destinationViewController;
        addRoomViewController.delegate = self;
    }
}

#pragma mark - KNAddRoomViewControllerDelegate

- (void)addRoomViewController:(KNAddRoomViewController *)addRoomViewController didFinishWithImage:(UIImage *)image
{
    [addRoomViewController.cropController.navigationController popToRootViewControllerAnimated:NO];
    [addRoomViewController dismissViewControllerAnimated:YES completion:nil];
    [addRoomViewController.navigationController popToViewController:self animated:YES];
    self.image = image;
    _imageView.image = image;
}

@end
