//
//  KNEditRoomViewController.h
//  KINO
//
//  Created by Aidian.Tang on 13-12-8.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WFRoom.h"

@class KNEditRoomViewController;

@protocol KNEditRoomViewControllerDelegate <NSObject>

- (void)editRoomViewControllerDidCancel:(KNEditRoomViewController *)editRoomViewController;
- (void)editRoomViewControllerDidSave:(KNEditRoomViewController *)editRoomViewController;

@end

@interface KNEditRoomViewController : UIViewController

@property (nonatomic, weak) id<KNEditRoomViewControllerDelegate> delegate;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) WFRoom *room;

@end
