//
//  KNColorPlate.m
//  KINO
//
//  Created by Aidian.Tang on 13-12-7.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import "KNColorPlate.h"
#import "CGPointExtension.h"
#import <QuartzCore/QuartzCore.h>

#define DRAGGABLE_VIEWS_COUNT 5

#define kRadiusRes 2

@interface KNColorPlate () {
    CGPoint _currentPoint;
    CGPoint _centerPoint;
    CGFloat _radius;
}

@property (nonatomic, strong) UIImageView *colorImageView;
@property (nonatomic, strong) UIImageView *selectorImageView;
@property (nonatomic, strong) UIView *activedDraggableView;

@end

@implementation KNColorPlate

- (id)initWithFrame:(CGRect)frame colorImage:(UIImage *)colorImage
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.colorImage = colorImage;
        [self setInitDatas];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setInitDatas];
}

#pragma mark - Private

- (void)setInitDatas
{
    CGRect frame = self.frame;
    _radius = frame.size.width / 2.;
    _centerPoint = CGPointMake(frame.size.width/ 2., frame.size.height / 2.);;
    _currentPoint = _centerPoint;
    
    self.layer.anchorPoint = CGPointMake(.5, .5);
    
    _isCirclePointsMode = NO;
    _is255Mode = NO;
    
    if (!_container) {
        self.container = [[UIView alloc] init];
        self.container.frame = self.bounds;
        self.container.clipsToBounds = YES;
        self.container.backgroundColor = [UIColor clearColor];
        self.container.opaque = YES;
        [self addSubview:_container];
        for (int idx = 0; idx < DRAGGABLE_VIEWS_COUNT; ++idx) {
            CGPoint position = self.center;
            UIImageView *draggableView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
            draggableView.layer.anchorPoint = CGPointMake(.5, .5);
            draggableView.image = [UIImage imageNamed:@"img_selector"];
            draggableView.userInteractionEnabled = YES;
            draggableView.bounds = (CGRect){{0, 0},{20, 20}};
            draggableView.center = position;
            self.activedDraggableView = draggableView;
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
            label.text = [NSString stringWithFormat:@"%d", idx + 1];
            label.backgroundColor = [UIColor clearColor];
            [draggableView addSubview:label];
            label.center = CGPointMake(5.5, 0);
            label.layer.name = label.text;
            CALayer *draggableLayer = draggableView.layer;
            draggableLayer.cornerRadius = 10;
            draggableLayer.sublayerTransform = CATransform3DMakeTranslation(10, 10, 0);
            UIPanGestureRecognizer *gestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                                action:@selector(didDragView:)];
            [draggableView addGestureRecognizer:gestureRecognizer];
            for (NSUInteger i = 0; i < 1; ++i) {
                CALayer *lineLayer = [CALayer layer];
                lineLayer.opacity = 0.7;
                [draggableLayer addSublayer:lineLayer];
            }
            [self.container addSubview:draggableView];
        }
        
        [CATransaction setDisableActions:YES];
        for (UIView *draggableView in self.container.subviews) {
            draggableView.backgroundColor = [UIColor clearColor];
            for (CALayer *lineLayer in draggableView.layer.sublayers) {
                if (!lineLayer.name) {
                    lineLayer.contents = nil;
                    lineLayer.backgroundColor = [UIColor blackColor].CGColor;
                    lineLayer.delegate = nil;
                }
            }
        }
        [CATransaction setDisableActions:NO];
        
        self.isCirclePointsMode = NO;
    }
}

- (void)updateLinesForView:(UIView *)draggableView
{
    [CATransaction setDisableActions:YES];
    NSArray *draggableViews = draggableView.superview.subviews;
    NSArray *lineLayers = draggableView.layer.sublayers;
    NSUInteger viewIndex = [draggableViews indexOfObject:draggableView];
    CGPoint pos = draggableView.center;
    
    for (NSUInteger i = 0; i < [lineLayers count]; ++i) {
        CALayer *lineLayer = [lineLayers objectAtIndex:i];
        if (lineLayer.name) {
            continue;
        }
        CGPoint target = CGPointZero;
        
        if (viewIndex + 1 < draggableViews.count) {
            UIView *otherDraggableView = [draggableViews objectAtIndex:viewIndex + 1];
            target = otherDraggableView.center;
            target.x -= pos.x;
            target.y -= pos.y;
        } else {
            UIView *otherDraggableView = [draggableViews objectAtIndex:0];
            target = otherDraggableView.center;
            target.x -= pos.x;
            target.y -= pos.y;
        }
        
        setLayerToLineFromAToB(lineLayer, CGPointZero, target, 2);
    }
    
    if (viewIndex == 0) {
        UIView *otherDraggableView = [draggableViews objectAtIndex:draggableViews.count - 1];
        CALayer *lineLayer = [otherDraggableView.layer.sublayers objectAtIndex:1];
        CGPoint target = otherDraggableView.center;
        target.x = pos.x - target.x;
        target.y = pos.y - target.y;
        setLayerToLineFromAToB(lineLayer, CGPointZero, target, 2);
    } else {
        UIView *otherDraggableView = [draggableViews objectAtIndex:viewIndex - 1];
        CALayer *lineLayer = [otherDraggableView.layer.sublayers objectAtIndex:1];
        CGPoint target = otherDraggableView.center;
        target.x = pos.x - target.x;
        target.y = pos.y - target.y;
        setLayerToLineFromAToB(lineLayer, CGPointZero, target, 2);
    }
    
    [CATransaction setDisableActions:NO];
}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
    if ([[layer valueForKey:@"alternate"] boolValue]) {
        CGContextMoveToPoint(ctx, layer.bounds.size.width, 0);
        CGContextAddLineToPoint(ctx, 0, layer.bounds.size.height);
    } else {
        CGContextMoveToPoint(ctx, 0, 0);
        CGContextAddLineToPoint(ctx, layer.bounds.size.width, layer.bounds.size.height);
    }
    CGContextSetStrokeColorWithColor(ctx, [UIColor blueColor].CGColor);
    CGContextSetLineWidth(ctx, 8);
    CGContextStrokePath(ctx);
}

- (void)didDragView:(UIPanGestureRecognizer *)gestureRecognizer
{
    CGPoint translation = [gestureRecognizer locationInView:self];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        self.activedDraggableView = gestureRecognizer.view;
        if (ccpDistance(translation, _centerPoint) > _radius) return;
        
        _currentPoint = translation;
        [self updateCirclePointPosition];
    } else if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        if (ccpDistance(translation, _centerPoint) > _radius)
        {
            _currentPoint =ccpAdd(_centerPoint,ccpMult(ccpNormalize(ccpSub(translation, _centerPoint)), _radius));
        }else {
            _currentPoint = translation;
        }
        [self updateCirclePointPosition];
    } else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        [self updatePointPosition:_currentPoint];
    }
    [self updateLinesForView:gestureRecognizer.view];
}

static void setLayerToLineFromAToB(CALayer *layer, CGPoint a, CGPoint b, CGFloat lineWidth)
{
    CGPoint center = { 0.5 * (a.x + b.x), 0.5 * (a.y + b.y) };
    CGFloat length = sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    CGFloat angle = atan2(a.y - b.y, a.x - b.x);
    
    layer.position = center;
    layer.bounds = (CGRect) { {0, 0}, { length + lineWidth - 20, lineWidth } };
    layer.transform = CATransform3DMakeRotation(angle, 0, 0, 1);
}

- (void)updatePointPosition:(CGPoint)postion
{
    if (!_isCirclePointsMode) {
        if (_selectorImageView.hidden) {
            _selectorImageView.hidden = NO;
        }
        _selectorImageView.center = postion;
    }
    _currentPoint = postion;
    
    if (!_is255Mode) {
        if (fabs(postion.x - _currentPoint.x) < .5 && fabs(postion.y - _currentPoint.y) < .5) {
            [self getPixelColorAtLocation:CGPointMake(postion.x * 2, postion.y * 2)];
        }
    } else {
        for (NSInteger i = 0; i <= 255; i++) {
            CGFloat height = (CGRectGetHeight(self.frame) - CGRectGetWidth(_selectorImageView.frame) - kRadiusRes * 2) / 255;
            if (CGRectContainsPoint(CGRectMake(0, CGRectGetWidth(_selectorImageView.frame) / 2. + kRadiusRes + i * height, CGRectGetWidth(self.frame), height), _currentPoint)) {
                if (_delegate && [_delegate respondsToSelector:@selector(colorPlate:didCircle:updateValue:)]) {
                    [_delegate colorPlate:self didCircle:[self.container.subviews indexOfObject:_activedDraggableView] updateValue:i];
                }
                NSLog(@"区域：%d", i);
                break;
            } else if (_currentPoint.y > CGRectGetHeight(self.frame) - CGRectGetWidth(_selectorImageView.frame) / 2. - kRadiusRes) {
                if (_delegate && [_delegate respondsToSelector:@selector(colorPlate:didCircle:updateValue:)]) {
                    [_delegate colorPlate:self didCircle:[self.container.subviews indexOfObject:_activedDraggableView] updateValue:255];
                }
                NSLog(@"区域：%d", 255);
                break;
            } else if (_currentPoint.y < CGRectGetWidth(_selectorImageView.frame) / 2. + kRadiusRes) {
                if (_delegate && [_delegate respondsToSelector:@selector(colorPlate:didCircle:updateValue:)]) {
                    [_delegate colorPlate:self didCircle:[self.container.subviews indexOfObject:_activedDraggableView] updateValue:0];
                }
                NSLog(@"区域：%d", 0);
                break;
            }
        }
    }
}

- (void)updateCirclePointPosition
{
    if (!_isCirclePointsMode) {
        return;
    }
    NSLog(@"%@", NSStringFromCGPoint(_currentPoint));
    _activedDraggableView.center = _currentPoint;
    NSInteger index = [self.container.subviews indexOfObject:_activedDraggableView];
    NSMutableArray *circlePoints = [NSMutableArray arrayWithArray:_circlePoints];
    [circlePoints replaceObjectAtIndex:index withObject:[NSValue valueWithCGPoint:_currentPoint]];
    self.circlePoints = circlePoints;
}

- (void)hideSelectorImageView
{
    _selectorImageView.hidden = YES;
}

#pragma Setter

- (void)setColorImage:(UIImage *)colorImage
{
    _colorImage = colorImage;
    if (!_colorImageView) {
        self.colorImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self insertSubview:_colorImageView atIndex:0];
    }
    self.colorImageView.image = _colorImage;
}

- (void)setSelectorImage:(UIImage *)selectorImage
{
    _selectorImage = selectorImage;
    if (!_selectorImageView) {
        self.selectorImageView = [[UIImageView alloc] initWithImage:_selectorImage];
        //_radius = (self.frame.size.width - self.selectorImageView.frame.size.width) / 2.;
        _radius = (self.frame.size.width - 1) / 2. - kRadiusRes;
        self.selectorImageView.layer.anchorPoint = CGPointMake(.5, .5);
        [self insertSubview:_selectorImageView aboveSubview:_colorImageView];
        _selectorImageView.hidden = YES;
    }
    self.selectorImageView.image = _selectorImage;
}

#pragma mark - Touch Event

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_isCirclePointsMode) {
        return;
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideSelectorImageView) object:nil];
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self];
    
    //NSLog(@"currentPoint = %@, %f", NSStringFromCGPoint(_currentPoint), ccpDistance(touchPoint, _centerPoint));
    
	if (ccpDistance(touchPoint, _centerPoint) > _radius) return;
    
    [self updatePointPosition:touchPoint];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"move");
    if (_isCirclePointsMode) {
        return;
    }
    
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self];
    
	if (ccpDistance(touchPoint, _centerPoint) > _radius)
	{
		touchPoint =ccpAdd(_centerPoint,ccpMult(ccpNormalize(ccpSub(touchPoint, _centerPoint)), _radius));
	} else {
		touchPoint = touchPoint;
	}
    
    [self updatePointPosition:touchPoint];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"end");
	//_currentPoint = _centerPoint;
    if (_isCirclePointsMode) {
        return;
    }
    
    [self updatePointPosition:_currentPoint];
    [self performSelector:@selector(hideSelectorImageView) withObject:nil afterDelay:2];
}

#pragma mark - Member

- (void)setIsCirclePointsMode:(BOOL)isCirclePointsMode
{
    self.container.hidden = !isCirclePointsMode;
    _isCirclePointsMode = isCirclePointsMode;
    if (isCirclePointsMode) {
        self.selectorImageView.hidden = YES;
        //_radius = (self.frame.size.width - 1) / 2.;
    } else {
        //_radius = (self.frame.size.width - 1) / 2. - 2.;
    }
}

#pragma mark RGB

- (CGContextRef) createARGBBitmapContextFromImage:(CGImageRef) inImage {
    
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    void *          bitmapData;
    int             bitmapByteCount;
    int             bitmapBytesPerRow;
    
    // Get image width, height. We'll use the entire image.
    size_t pixelsWide = CGImageGetWidth(inImage);
    size_t pixelsHigh = CGImageGetHeight(inImage);
    
    // Declare the number of bytes per row. Each pixel in the bitmap in this
    // example is represented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    bitmapBytesPerRow   = (pixelsWide * 4);
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
    // Use the generic RGB color space.
    colorSpace = CGColorSpaceCreateDeviceRGB();
    
    if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
        return NULL;
    }
    
    // Allocate memory for image data. This is the destination in memory
    // where any drawing to the bitmap context will be rendered.
    bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Memory not allocated!");
        CGColorSpaceRelease( colorSpace );
        return NULL;
    }
    
    // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
    // per component. Regardless of what the source image format is
    // (CMYK, Grayscale, and so on) it will be converted over to the format
    // specified here by CGBitmapContextCreate.
    context = CGBitmapContextCreate (bitmapData,
                                     pixelsWide,
                                     pixelsHigh,
                                     8,      // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaPremultipliedFirst);
    if (context == NULL)
    {
        free (bitmapData);
        fprintf (stderr, "Context not created!");
    }
    // Make sure and release colorspace before returning
    CGColorSpaceRelease( colorSpace );
    
    return context;
}

- (NSArray*)getRGBAsFromImage:(UIImage*)image atX:(int)xx andY:(int)yy count:(int)count
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:count];
    
    // First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    // Now your rawData contains the image data in the RGBA8888 pixel format.
    int byteIndex = (bytesPerRow * yy) + xx * bytesPerPixel;
    for (int ii = 0 ; ii < count ; ++ii)
    {
        CGFloat red   = (rawData[byteIndex]     * 1.0) / 255.0;
        CGFloat green = (rawData[byteIndex + 1] * 1.0) / 255.0;
        CGFloat blue  = (rawData[byteIndex + 2] * 1.0) / 255.0;
        CGFloat alpha = (rawData[byteIndex + 3] * 1.0) / 255.0;
        byteIndex += 4;
        
        UIColor *acolor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
        [result addObject:acolor];
    }
    
    free(rawData);
    
    return result;
}

- (UIColor*) getPixelColorAtLocation:(CGPoint)point {
    
    @autoreleasepool {
        
        UIColor* color = nil;
        CGImageRef inImage = _colorImageView.image.CGImage;
        // Create off screen bitmap context to draw the image into. Format ARGB is 4 bytes for each pixel: Alpa, Red, Green, Blue
        CGContextRef cgctx = [self createARGBBitmapContextFromImage:inImage];
        if (cgctx == NULL) { return nil;  }
        
        size_t w = CGImageGetWidth(inImage);
        size_t h = CGImageGetHeight(inImage);
        CGRect rect = {{0,0},{w,h}};
        
        // Draw the image to the bitmap context. Once we draw, the memory
        // allocated for the context for rendering will then contain the
        // raw image data in the specified color space.
        CGContextDrawImage(cgctx, rect, inImage);
        
        // Now we can get a pointer to the image data associated with the bitmap
        // context.
        unsigned char* data = CGBitmapContextGetData (cgctx);
        if (data != NULL) {
            //offset locates the pixel in the data from x,y.
            //4 for 4 bytes of data per pixel, w is width of one row of data.
            @try {
                int offset = 4*((w*round(point.y))+round(point.x));
                //NSLog(@"offset: %d", offset);
                int alpha =  data[offset];
                int red = data[offset+1];
                int green = data[offset+2];
                int blue = data[offset+3];
                //NSLog(@"offset: %i colors: RGB A %i %i %i  %i",offset,red,green,blue,alpha);
                color = [UIColor colorWithRed:(red/255.0f) green:(green/255.0f) blue:(blue/255.0f) alpha:(alpha/255.0f)];
                
                if (_isCirclePointsMode) {
                    if (_delegate && [_delegate respondsToSelector:@selector(colorPlate:didCircle:updateRed:green:blue:)]) {
                        [_delegate colorPlate:self didCircle:[self.container.subviews indexOfObject:_activedDraggableView] updateRed:red green:green blue:blue];
                    }
                } else {
                    if (_delegate && [_delegate respondsToSelector:@selector(colorPlate:didSelectorUpdateRed:green:blue:)]) {
                        [_delegate colorPlate:self didSelectorUpdateRed:red green:green blue:blue];
                    }
                }
            }
            @catch (NSException * e) {
                //NSLog(@"%@",[e reason]);
            }
            @finally {
            }
            
        }
        // When finished, release the context
        CGContextRelease(cgctx);
        // Free image data memory for the context
        if (data) { free(data); }
        
        return color;
        
    }
}

- (void)setCirclePointPositions:(NSArray *)postions
{
    for (NSInteger idx = 0;idx < self.container.subviews.count; idx++) {
        UIView *draggableView = [self.container.subviews objectAtIndex:idx];
        CGPoint position = [[postions objectAtIndex:idx] CGPointValue];
        draggableView.center = position;
        [self updateLinesForView:draggableView];
    }
    self.circlePoints = postions;
}

@end
