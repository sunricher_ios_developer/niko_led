//
//  WFRouterListCell.h
//  WFEasyLighting
//
//  Created by Aidian.Tang on 13-11-11.
//  Copyright (c) 2013年 WFWinfires. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WFRouterListCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *ssidLab;
@property (nonatomic, strong) IBOutlet UILabel *securityLab;
@property (nonatomic, strong) IBOutlet UIImageView *lockImageView;

@end
