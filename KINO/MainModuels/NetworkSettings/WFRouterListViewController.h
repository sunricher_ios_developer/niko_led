//
//  WFRouterListViewController.h
//  WFEasyLighting
//
//  Created by Aidian.Tang on 13-11-11.
//  Copyright (c) 2013年 WFWinfires. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WFNetwork.h"

@class WFRouterListViewController;

@protocol WFRouterListViewControllerDelegate <NSObject>

- (void)routerListViewControllerDidCancel:(WFRouterListViewController *)routerListViewController;

@end

@interface WFRouterListViewController : UIViewController

@property (nonatomic, weak) id<WFRouterListViewControllerDelegate> delegate;
@property (nonatomic, assign) BOOL psdSending;
@property (nonatomic, strong) IBOutlet UIView *routerPsdContainView;
@property (nonatomic, copy) NSString *router;

- (void)connectRouter;

@end
