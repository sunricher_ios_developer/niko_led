//
//  WFRouterListViewController.m
//  WFEasyLighting
//
//  Created by Aidian.Tang on 13-11-11.
//  Copyright (c) 2013年 WFWinfires. All rights reserved.
//

#import "WFRouterListViewController.h"
#import "WFRouterListCell.h"

@interface WFRouterListViewController () <WFNetworkClientDelegate, UITextFieldDelegate> {
    BOOL _isOK;
    BOOL _networkReceive;
    BOOL _staModeSending;
    BOOL _atNetpSending;
    
    NSInteger _holdTimerCount;
    
    BOOL _deviceScaning;
    BOOL _routerScanErrorShowed;
}

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UITextField *psdTextField;
@property (nonatomic, strong) IBOutlet UIView *waitContainer;

@property (nonatomic, strong) NSArray *routers;

@property (nonatomic, strong) NSTimer *scanTimer;
@property (nonatomic, strong) NSTimer *holdRouterTimer;

@property (nonatomic, assign) BOOL connectRouterNeeded;

@end

@implementation WFRouterListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    WFNetwork *netWork = [WFNetworkClient sharedNetworkClient].setupNetwork;
    [netWork refreshIP];
    if (!netWork.ipAddressGetFailed) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        _isOK = NO;
        _networkReceive = NO;
        _staModeSending = NO;
        self.routers = nil;
        _deviceScaning = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scanComplete:) name:kDeviceScanDidCompleted object:nil];
        [WFNetworkClient sharedNetworkClient].delegate = self;
        [[WFNetworkClient sharedNetworkClient] startDeviceScaning];
    }
    
}

- (void)scanComplete:(id)sender
{
    _deviceScaning = NO;
    if ([WFNetworkClient sharedNetworkClient].devices.count > 0) {
        _isOK = NO;
        [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:[@"+ok" dataUsingEncoding:NSASCIIStringEncoding] withTimeout:3 tryCount:3];
    } else {
        _routerScanErrorShowed = YES;
        
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"No RGB(W) controller found. Connect to the correct WiFi network." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _routerScanErrorShowed = NO;
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];

    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIView *view = [_routerPsdContainView viewWithTag:100];
    [_routerPsdContainView insertSubview:[[UIImageView alloc] initWithImage:[UIView backgroundGradientImageWithSize:_routerPsdContainView.frame.size]] atIndex:0];
    view.layer.cornerRadius = 5.;
    view = [_waitContainer viewWithTag:100];
    view.layer.cornerRadius = 5.;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action

- (IBAction)back:(id)sender
{
    [_holdRouterTimer invalidate];
    self.holdRouterTimer = nil;
    [_scanTimer invalidate];
    self.scanTimer = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Member

- (void)connectRouter
{
    BOOL isOpen = NO;
    NSArray *datas = [_router componentsSeparatedByString:@","];
    if (datas.count >= 5) {
        NSString *security = [datas objectAtIndex:3];
        NSArray *securities = [security componentsSeparatedByString:@"/"];
        if (securities.count > 0) {
            NSString *security1 = [securities objectAtIndex:0];
            if ([security1 rangeOfString:@"OPEN"].length > 0 || [security1 rangeOfString:@"NONE"].length > 0) {
                isOpen = YES;
            }
        }
    }
    if (!isOpen) {
        [self sendPsd];
    } else {
        _psdSending = YES;
        NSMutableData *data = [NSMutableData dataWithData:[[@"AT+WSKEY=" stringByAppendingFormat:@"%@,%@,%@", @"OPEN", @"NONE", @""] dataUsingEncoding:NSASCIIStringEncoding]];
        unsigned int intValue = 13;
        [data appendData:[NSData dataWithBytes:&intValue length:1]];
        [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:3 tryCount:3];
    }
    self.connectRouterNeeded = NO;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSInteger numberOfRowsInSection = [_routers count];
    
    return numberOfRowsInSection;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"WFRouterListCell";
    WFRouterListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[WFRouterListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    
    NSString *router = [_routers objectAtIndex:indexPath.row];
    NSArray *datas = [router componentsSeparatedByString:@","];
    if (datas.count > 3) {
        cell.ssidLab.text = [datas objectAtIndex:1];
        if(cell.ssidLab.text.length == 0){
            cell.ssidLab.text = @"Unknown";
        }
        cell.securityLab.text = [datas objectAtIndex:3];
        NSString *security = [datas objectAtIndex:3];
        if ([security rangeOfString:@"OPEN"].length > 0 || [security rangeOfString:@"NONE"].length > 0) {
            cell.lockImageView.hidden = YES;
        } else {
            cell.lockImageView.hidden = NO;
        }
    } else {
        cell.ssidLab.text = nil;
        cell.securityLab.text = nil;
        cell.lockImageView.hidden = YES;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIDevice currentDevice] deviceFamily] == UIDeviceFamilyiPad) {
        return 48;
    }
    
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *router = [_routers objectAtIndex:indexPath.row];
    self.router = router;
    [self checkRouter];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.psdTextField resignFirstResponder];
}

#pragma mark - Private

- (void)sendPsd
{
    NSString *psd = _psdTextField.text;
    NSArray *datas = [_router componentsSeparatedByString:@","];
    if (datas.count > 3) {
        NSString *security = [datas objectAtIndex:3];
        if ([security rangeOfString:@"WEP"].length > 0) {
            NSString *security1 = @"SHARED";
            _psdSending = YES;
            _routerPsdContainView.hidden = YES;

            NSString *security2 = @"WEP-H";

            NSMutableData *data = [NSMutableData dataWithData:[[@"AT+WSKEY=" stringByAppendingFormat:@"%@,%@,%@", security1, security2, psd] dataUsingEncoding:NSASCIIStringEncoding]];
            unsigned int intValue = 13;
            [data appendData:[NSData dataWithBytes:&intValue length:1]];
            [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:3 tryCount:3];
        } else {
            NSArray *securities = [security componentsSeparatedByString:@"/"];
            if (securities.count > 0) {
                NSString *security1 = [securities objectAtIndex:0];
                if ([security1 rangeOfString:@"OPEN"].length > 0 || [security1 rangeOfString:@"NONE"].length > 0) {
                    psd = @"";
                } else {
                    if (psd.length == 0) {
                        [MBProgressHUD showError:@"Please input password" toView:nil];
                        return;
                    }
                }
                
                _psdSending = YES;
                _routerPsdContainView.hidden = YES;
                
                if ([security1 rangeOfString:@"OPEN"].length > 0 || [security1 rangeOfString:@"NONE"].length > 0) {
                    security1 = @"OPEN";
                } else if ([security1 rangeOfString:@"SHARED"].length > 0) {
                    security1 = @"SHARED";
                } else if ([security1 rangeOfString:@"WPA2PSK"].length > 0) {
                    security1 = @"WPA2PSK";
                } else if ([security1 rangeOfString:@"WPAPSK"].length > 0) {
                    security1 = @"WPAPSK";
                }
                
                NSString *security2 = [securities objectAtIndex:1];
                if ([security2 rangeOfString:@"NONE"].length > 0) {
                    security2= @"NONE";
                } else if ([security2 rangeOfString:@"WEP-H"].length > 0) {
                    security2= @"WEP-H";
                } else if ([security2 rangeOfString:@"AES"].length > 0) {
                    security2= @"AES";
                } else if ([security2 rangeOfString:@"TKIP"].length > 0) {
                    security2= @"TKIP";
                }
                
                NSMutableData *data = [NSMutableData dataWithData:[[@"AT+WSKEY=" stringByAppendingFormat:@"%@,%@,%@", security1, security2, psd] dataUsingEncoding:NSASCIIStringEncoding]];
                unsigned int intValue = 13;
                [data appendData:[NSData dataWithBytes:&intValue length:1]];
                [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:3 tryCount:3];
            }
        }
    }
}

- (void)sendSSID
{
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    self.connectRouterNeeded = YES;
    _networkReceive = NO;
    NSArray *datas = [_router componentsSeparatedByString:@","];
    if (datas.count > 3) {
        _psdSending = NO;
        NSString *ssid = [datas objectAtIndex:1];
        //NSString *security = [datas objectAtIndex:3];
        NSMutableData *data = [NSMutableData dataWithData:[[@"AT+WSSSID=" stringByAppendingString:ssid] dataUsingEncoding:NSASCIIStringEncoding]];
        unsigned int intValue = 13;
        [data appendData:[NSData dataWithBytes:&intValue length:1]];
        [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:3 tryCount:3];
    }
    [self sendPsd];
    
    NSMutableData *data = [NSMutableData dataWithData:[@"AT+WMODE=STA" dataUsingEncoding:NSASCIIStringEncoding]];
    unsigned int intValue = 13;
    [data appendData:[NSData dataWithBytes:&intValue length:1]];
    [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:3 tryCount:3];

    NSMutableData *data2 = [NSMutableData dataWithData:[@"AT+NETP=UDP,SERVER,8899,10.10.10.1" dataUsingEncoding:NSASCIIStringEncoding]];
    [data appendData:[NSData dataWithBytes:&intValue length:1]];
    [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data2 withTimeout:2 tryCount:3];
    
    NSMutableData *data3 = [NSMutableData dataWithData:[@"AT+Z" dataUsingEncoding:NSASCIIStringEncoding]];
    [data appendData:[NSData dataWithBytes:&intValue length:1]];
    [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data3 withTimeout:2 tryCount:3];
    
    WFNetworkClient *networkClient = [WFNetworkClient sharedNetworkClient];
    [networkClient.activeNetwork inActive];
    [networkClient.activeNetwork refreshIP];
    [networkClient archiveNetwork:networkClient.activeNetwork];
    [networkClient.activeNetwork active];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.tableView animated:YES];
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"RGB(W) controller successfully connected to your network. Now connect your mobile device to your network." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
    });
    
}

- (BOOL)isOpen
{
    BOOL isOpen = NO;
    NSArray *datas = [_router componentsSeparatedByString:@","];
    if (datas.count >= 5) {
        NSString *security = [datas objectAtIndex:3];
        NSArray *securities = [security componentsSeparatedByString:@"/"];
        if (securities.count > 0) {
            NSString *security1 = [securities objectAtIndex:0];
            if ([security1 rangeOfString:@"OPEN"].length > 0 || [security1 rangeOfString:@"NONE"].length > 0) {
                isOpen = YES;
            }
        }
    }
    return isOpen;
}

- (void)checkRouter
{
    if (![self isOpen]) {
        self.routerPsdContainView.hidden = NO;
    } else {
        [self sendSSID];
    }
}

#pragma mark - Action

- (IBAction)cancelDidInputed:(UIButton *)sender
{
//    if (_delegate && [_delegate respondsToSelector:@selector(routerListViewControllerDidCancel:)]) {
//        [_delegate routerListViewControllerDidCancel:self];
//    }
    _routerPsdContainView.hidden = YES;
}

- (IBAction)pasDidInputed:(UIButton *)sender
{
    [_psdTextField resignFirstResponder];
    if (sender.tag == 1) {
        _routerPsdContainView.hidden = YES;
        return;
    }
    
    if (![self isOpen]) {
        if (_psdTextField.text.length == 0) {
            [MBProgressHUD showError:@"Please input password" toView:nil];
            return;
        }
    }
    [self sendSSID];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - WFNetworkDelegate

- (void)networkDidTimeout:(WFNetwork *)network
{
    if (!_isOK) {
        [self showAlter];
    }
    _waitContainer.hidden = YES;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)scanTimeOut
{
    [self showAlter];
    _waitContainer.hidden = YES;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)showAlter{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"No WiFi networks found. Ensure that the RGB(W) controller is in range with a WiFi network." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertVC addAction:okAction];
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - Hold Timer

- (void)holdRouterTimer:(NSTimer *)timer
{
    NSMutableData *data = [NSMutableData dataWithData:[@"AT+W" dataUsingEncoding:NSASCIIStringEncoding]];
    unsigned int intValue = 13;
    [data appendData:[NSData dataWithBytes:&intValue length:1]];
    [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:3 tryCount:3];
    
    _holdTimerCount++;
    if (_holdTimerCount == 6) {
        UIAlertController *alterVC = [UIAlertController alertControllerWithTitle:nil message:@"Input time out,do you want to give up the current operation?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alterVC addAction:cancelAction];
        [alterVC addAction:okAction];
        [self presentViewController:alterVC animated:YES completion:nil];
    }
}

- (void)reloadRouterDatas
{
    //NSLog(@"routerList = %@", routerList);
    if (_routers.count > 0) {
        [self.tableView reloadData];
        if (!_holdRouterTimer) {
            _holdTimerCount = 0;
            self.holdRouterTimer = [NSTimer timerWithTimeInterval:10 target:self selector:@selector(holdRouterTimer:) userInfo:nil repeats:YES];
        }
    }
}

#pragma mark - WFNetworkClientDelegate

- (void)networkClient:(WFNetworkClient *)networkClient didScanedDeviceFailed:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)networkClient:(WFNetworkClient *)networkClient didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext
{
    
    NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"settings %@", msg);
    
    if (!_isOK && [msg isEqualToString:@"+ok"]) {
        _isOK = YES;
        NSMutableData *data = [NSMutableData dataWithData:[@"AT+WSCAN" dataUsingEncoding:NSASCIIStringEncoding]];
        unsigned int intValue = 13;
        [data appendData:[NSData dataWithBytes:&intValue length:1]];
        [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:3 tryCount:3];
        self.scanTimer = [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(scanTimeOut) userInfo:nil repeats:NO];
    }
    
    if (_networkReceive) {
        NSRange range = [msg rangeOfString:@"Ch,SSID,BSSID,Security,Indicator"];
        NSMutableArray *routerList = [NSMutableArray arrayWithArray:_routers];
        NSArray *routerDatas = [msg componentsSeparatedByString:@","];
        if (range.length == 0 && [routerDatas count] >= 5) {
            BOOL isExited = NO;
            for (NSString *rou in _routers) {
                NSArray *temRouterDatas = [rou componentsSeparatedByString:@","];
                if ([[temRouterDatas objectAtIndex:1] isEqualToString:[routerDatas objectAtIndex:1]] && [[temRouterDatas objectAtIndex:2] isEqualToString:[routerDatas objectAtIndex:2]]) {
                    isExited = YES;
                    break;
                }
            }
            if (!isExited) {
                [routerList addObject:msg];
            }
        }
        self.routers = routerList;
        NSLog(@"显示:%@", _routers);
        [self reloadRouterDatas];
    }
    
    NSRange range = [msg rangeOfString:@"Ch,SSID,BSSID,Security,Indicator"];
    if (range.length > 0) {
        //一次返回一个router
        [_scanTimer invalidate];
        _networkReceive = YES;
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        _waitContainer.hidden = YES;
    } else {
        //一次返回所有router
        range = [msg rangeOfString:@"Ch,SSID,BSSID,Security,Siganl(%)W-Mode,ExtCH,NT,WPS,DPID"];
        if (range.length > 0) {
            [_scanTimer invalidate];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            _waitContainer.hidden = YES;
            
            if (range.location != 0) {
                range.length += range.location;
                range.location = 0;
            }
            msg = [msg substringFromIndex:range.length];
            
            NSArray *routerList = [msg componentsSeparatedByString:@"\n"];
            NSLog(@"%@", routerList);
            NSMutableArray *rList = [NSMutableArray arrayWithArray:routerList];
            for (NSString *router in routerList) {
                NSLog(@"router = %@", router);
                NSString *rou = [router stringByReplacingOccurrencesOfString:@" " withString:@""];
                rou = [router stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                NSRange range = [router rangeOfString:@","];
                if (rou.length == 0 || range.length == 0) {
                    //                NSInteger removedIndex = -1;
                    //                for (NSString *str in rList) {
                    //                    if ([str isEqualToString:router]) {
                    //                        removedIndex = [rList indexOfObject:str];
                    //                        break;
                    //                    }
                    //                }
                    //                [rList removeObjectAtIndex:removedIndex];
                    [rList removeObject:router];
                }
            }
            self.routers = rList;
            [self reloadRouterDatas];
        }
    }
    
//    if (_atNetpSending) {
//        NSRange range = [msg rangeOfString:@"+ok"];
//        if (range.length > 0) {
//            _atNetpSending = NO;
//            NSMutableData *data = [NSMutableData dataWithData:[@"AT+Z" dataUsingEncoding:NSASCIIStringEncoding]];
//            unsigned int intValue = 13;
//            [data appendData:[NSData dataWithBytes:&intValue length:1]];
//            [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:2 tryCount:3];
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"RGB(W) controller successfully connected to your network. Now connect your mobile device to your network." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alertView show];
//            
//            WFNetworkClient *networkClient = [WFNetworkClient sharedNetworkClient];
//            [networkClient.activeNetwork inActive];
//            [networkClient.activeNetwork refreshIP];
//            [networkClient archiveNetwork:networkClient.activeNetwork];
//            [networkClient.activeNetwork active];
//            [self.navigationController popToRootViewControllerAnimated:YES];
//        }
//    } else if (_staModeSending) {
//        NSRange range = [msg rangeOfString:@"+ok"];
//        if (range.length > 0) {
//            _networkReceive = NO;
//            _staModeSending = NO;
//            _atNetpSending = YES;
//            
//            NSMutableData *data = [NSMutableData dataWithData:[@"AT+NETP=UDP,SERVER,8899,10.10.10.1" dataUsingEncoding:NSASCIIStringEncoding]];
//            unsigned int intValue = 13;
//            [data appendData:[NSData dataWithBytes:&intValue length:1]];
//            [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:2 tryCount:3];
//        }
//    } else if (_psdSending) {
//        NSRange range = [msg rangeOfString:@"+ok"];
//        if (range.length > 0) {
//            _staModeSending = YES;
//            _psdSending = NO;
//            NSMutableData *data = [NSMutableData dataWithData:[@"AT+WMODE=STA" dataUsingEncoding:NSASCIIStringEncoding]];
//            unsigned int intValue = 13;
//            [data appendData:[NSData dataWithBytes:&intValue length:1]];
//            [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:3 tryCount:3];
//        }
   // } //else if (self.connectRouterNeeded && self.view.superview) {
//        NSRange range = [msg rangeOfString:@"+ok"];
//        if (range.length > 0) {
//            [self sendPsd];
//        }
//    }
}

@end
