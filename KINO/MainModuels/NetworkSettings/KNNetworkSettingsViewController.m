//
//  KNNetworkSettingsViewController.m
//  KINO
//
//  Created by Aidian.Tang on 13-12-8.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import "KNNetworkSettingsViewController.h"
#import "KNChangeSSIDViewController.h"
#import <SystemConfiguration/CaptiveNetwork.h>

@interface KNNetworkSettingsViewController () <WFNetworkClientDelegate> {
    BOOL _isNetWorkOK;
    BOOL _isOK;
    BOOL _alertShowed;
    BOOL _ssidReseting;
    BOOL _ssidResetSuccessShowed;
}

@property (nonatomic, strong) IBOutlet UISwitch *autoLinkSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *wifiSwitch;
@property (nonatomic, strong) KNAlertViewController *deleteAlertViewController;

@property (nonatomic, strong) IBOutlet UISwitch *factoryRestoredSwt;
@property (nonatomic, strong) NSTimer *networkOverTimer;
@property (nonatomic, strong) KNChangeSSIDViewController *changeSSIDViewController;

@property (nonatomic, copy) NSString *SSID;

@end

@implementation KNNetworkSettingsViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _autoLinkSwitch.on = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 检查是否需要更改SSID

- (BOOL)checkSSID
{
    NSString *ssid = [[self fetchSSIDInfo] objectForKey:@"SSID"];
    //ssid = @"niko rgb";
    if ([ssid caseInsensitiveCompare:@"NIKO RGB"] == NSOrderedSame) {
        KNChangeSSIDViewController *changeSSIDViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"KNChangeSSIDViewController"];
        changeSSIDViewController.view.frame = self.view.bounds;
        [changeSSIDViewController showMessage:@"Please Change your SSID:" inView:self.view withCompletion:^(BOOL isOk, NSString *SSID) {
            if (isOk) {
                _ssidReseting = YES;
                _ssidResetSuccessShowed = NO;
                self.SSID = SSID;
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceScanDidCompleted:) name:kDeviceScanDidCompleted object:nil];
                [WFNetworkClient sharedNetworkClient].delegate = self;
                [[WFNetworkClient sharedNetworkClient] startDeviceScaning];
            }
        }];
        self.changeSSIDViewController = changeSSIDViewController;
        return YES;
    }
    
    return NO;
}

#pragma mark Get SSID

- (id)fetchSSIDInfo {
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    NSLog(@"Supported interfaces: %@", ifs);
    id info = nil;
    for (NSString *ifnam in ifs) {
        info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        NSLog(@"%@ => %@", ifnam, info);
        if (info && [info count]) { break; }
    }
    return info;
}

#pragma mark - Action

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)value:(UISwitch *)swt
{
    NSLog(@"swt %d", swt.on);
    _ssidReseting = NO;
    
    if (swt.tag == 0) {
        if (swt.on) {
            KNAlertViewController *deleteAlertViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"KNAlertViewController"];
            deleteAlertViewController.view.frame = self.view.bounds;
            [deleteAlertViewController showMessage:@"Is your mobile device connected to the RGB(W) controller?" inView:self.view withCompletion:^(BOOL isOk) {
                if (isOk) {
                    [self performSegueWithIdentifier:@"toWFRouterListViewController" sender:nil];
                }
                swt.on = isOk;
            }];
            self.deleteAlertViewController = deleteAlertViewController;
        } else {
            WFNetworkClient *networkClient = [WFNetworkClient sharedNetworkClient];
            [networkClient.activeNetwork inActive];
            [networkClient archiveNetwork:networkClient.activeNetwork];
        }
        
    } else if (swt.tag == 1) {
        if (swt.on) {
            // 直连前，如果SSID 是KINO RGB就必须更改
            if ([self checkSSID]) {
                _wifiSwitch.on = NO;
                return;
            }
            
            KNAlertViewController *deleteAlertViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"KNAlertViewController"];
            deleteAlertViewController.view.frame = self.view.bounds;
            [deleteAlertViewController showMessage:@"Is your mobile device connected to the RGB(W) controller?" inView:self.view withCompletion:^(BOOL isOk) {
                if (isOk) {
                    WFNetworkClient *networkClient = [WFNetworkClient sharedNetworkClient];
                    [networkClient.activeNetwork inActive];
                    [networkClient.activeNetwork refreshIP];
                    [networkClient archiveNetwork:networkClient.activeNetwork];
                    [WFNetworkClient sharedNetworkClient].delegate = nil;
                    [networkClient startDeviceScaning];
                    [self back:nil];
                }
                swt.on = isOk;
            }];
            self.deleteAlertViewController = deleteAlertViewController;
        } else {
            WFNetworkClient *networkClient = [WFNetworkClient sharedNetworkClient];
            [networkClient.activeNetwork inActive];
            [networkClient archiveNetwork:networkClient.activeNetwork];
        }
    } else {
        if (swt.on) {
            KNAlertViewController *deleteAlertViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"KNAlertViewController"];
            deleteAlertViewController.view.frame = self.view.bounds;
            [deleteAlertViewController showMessage:@"Restore to factory settings. Are you sure?" inView:self.view withCompletion:^(BOOL isOk) {
                if (isOk) {
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    _isNetWorkOK = NO;
                    _isOK = NO;
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceScanDidCompleted:) name:kDeviceScanDidCompleted object:nil];
                    [WFNetworkClient sharedNetworkClient].delegate = self;
                    [[WFNetworkClient sharedNetworkClient] startDeviceScaning];
                }
                swt.on = isOk;
            }];
            self.deleteAlertViewController = deleteAlertViewController;
        }
    }
}

- (void)deviceScanDidCompleted:(id)sender
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if ([WFNetworkClient sharedNetworkClient].devices.count == 0) {
        if ([[WFNetworkClient sharedNetworkClient].delegate isEqual:self] && !_alertShowed) {
            [self networkDidOverTimer:nil];
        }
    } else {
        _isNetWorkOK = YES;
        _isOK = NO;
        if (_networkOverTimer) {
            [_networkOverTimer invalidate];
            self.networkOverTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(networkDidOverTimer:) userInfo:nil repeats:NO];
        }
        [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:[@"+ok" dataUsingEncoding:NSASCIIStringEncoding] withTimeout:3 tryCount:3];
    }
}

- (void)networkDidOverTimer:(NSTimer *)timer
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    _alertShowed = YES;
    
    if (_ssidReseting) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"Failed to change your SSID,please try again!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _alertShowed = NO;
        }];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
        
    } else {
        _factoryRestoredSwt.on = NO;
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"Restore to factory settings failed!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _alertShowed = NO;
        }];
        [alertVC addAction:okAction];
        [self presentViewController:alertVC animated:YES completion:nil];
    }
}

#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toWFRouterListViewController"]) {
        
    }
}

#pragma mark 

- (void)networkClient:(WFNetworkClient *)networkClient didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext
{
    NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"settings %@", msg);
    
    if (!_isOK && [msg isEqualToString:@"+ok"]) {
        _isOK = YES;
        
        if (_ssidReseting) {
            NSMutableData *data = [NSMutableData dataWithData:[[NSString stringWithFormat:@"AT+WAP=11BGN,%@,Auto", _SSID] dataUsingEncoding:NSASCIIStringEncoding]];
            unsigned int intValue = 13;
            [data appendData:[NSData dataWithBytes:&intValue length:1]];
            [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:3 tryCount:3];
        } else {
            NSMutableData *data = [NSMutableData dataWithData:[@"AT+RELD" dataUsingEncoding:NSASCIIStringEncoding]];
            unsigned int intValue = 13;
            [data appendData:[NSData dataWithBytes:&intValue length:1]];
            [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:3 tryCount:3];
        }
        
        if (_networkOverTimer) {
            [_networkOverTimer invalidate];
            self.networkOverTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(networkDidOverTimer:) userInfo:nil repeats:NO];
        }
    } else {
        if (_ssidReseting) {
            NSRange range = [msg rangeOfString:@"+ok"];
            if (range.length > 0) {
                _ssidReseting = NO;
                
                if (_networkOverTimer) {
                    [_networkOverTimer invalidate];
                }
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                NSMutableData *data = [NSMutableData dataWithData:[@"AT+Z" dataUsingEncoding:NSASCIIStringEncoding]];
                unsigned int intValue = 13;
                [data appendData:[NSData dataWithBytes:&intValue length:1]];
                [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:2 tryCount:3];
                [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:2 tryCount:3];
                [[WFNetworkClient sharedNetworkClient].setupNetwork sendData:data withTimeout:2 tryCount:3];

                if (!_ssidResetSuccessShowed) {
                    _ssidResetSuccessShowed = YES;
                    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"Successfully change your SSID!" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alertVC addAction:okAction];
                    [self presentViewController:alertVC animated:YES completion:nil];
                }
            }
        } else {
            NSRange range = [msg rangeOfString:@"+ok=rebooting"];
            if (range.length > 0) {
                if (_networkOverTimer) {
                    [_networkOverTimer invalidate];
                }
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                _factoryRestoredSwt.on = NO;
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"WiFi parameters successfully restored to factory settings. Connect your mobile device to the ‘Niko RGB’ WiFi network." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertVC addAction:okAction];
                [self presentViewController:alertVC animated:YES completion:nil];
            }
        }
    }
}

- (void)networkDidTimeout:(WFNetwork *)network
{
    
}

@end
