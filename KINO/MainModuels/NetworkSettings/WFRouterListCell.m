//
//  WFRouterListCell.m
//  WFEasyLighting
//
//  Created by Aidian.Tang on 13-11-11.
//  Copyright (c) 2013年 WFWinfires. All rights reserved.
//

#import "WFRouterListCell.h"

@interface WFRouterListCell ()


@end

@implementation WFRouterListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
