//
//  KNAddRoomViewController.h
//  KINO
//
//  Created by Aidian.Tang on 13-12-8.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKImageCropViewController.h"

@class KNAddRoomViewController;

@protocol KNAddRoomViewControllerDelegate <NSObject>

- (void)addRoomViewController:(KNAddRoomViewController *)addRoomViewController didFinishWithImage:(UIImage *)croppedImage;

@end

@interface KNAddRoomViewController : UIViewController

@property (nonatomic, weak) id<KNAddRoomViewControllerDelegate> delegate;
@property (nonatomic, strong) GKImageCropViewController *cropController;

@end
