//
//  KNViewController.m
//  KINO
//
//  Created by Aidian.Tang on 13-12-7.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import "KNHomeViewController.h"
#import "WFLoadingViewController.h"
#import "KNTutorialViewController.h"
#import "KNAddRoomViewController.h"
#import "KNEditRoomViewController.h"
#import "KNEditRoomViewController.h"
#import "CBAutoScrollLabel.h"

#import "KNColorPlate.h"

#import "KNAppDelegate.h"

#import "WFRoom.h"

#define kDividerHeight  45

#define kMinWBrightnessValue        0   //0x00
#define kMaxWBrightnessValue        100 //0x64

#define kMinRGBBrightnessValue      1   //0x00
#define kMaxRGBBrightnessValue      100 //0x64

#define kMinRunValue                60   //0x00
#define kMaxRunValue                7200 //0x64

#define kIsUp                       @"isUp"

@interface KNHomeViewController () <WFLoadingViewControllerDelegate, KNTutorialViewControllerDelegate, UIScrollViewDelegate, KNAddRoomViewControllerDelegate, KNColorPlateDelegate, KNEditRoomViewControllerDelegate, KNEditRoomViewControllerDelegate> {
    BOOL        _roomShowed;
    CGRect      _onOffBtnOrgFrame;
    NSInteger   _wBrightnessValue;
    NSInteger   _rgbBrightnessValue;
    NSInteger   _runValue;
    WFRoomType   _roomType;
    
    BOOL        _allRoomSelected;
    
    NSInteger _oldRgbBrightnessValue;
    NSInteger _oldwRgbBrightnessValue;
    NSInteger _oldCurWRgbBrightnessValue;
    
    BOOL    _wRGBOperateToZearo;//wrgb手动减到0，就不跟着rgb动
    BOOL    _isRrriveLimited;//wrgb和rgb是否到达限定数字，也就是按wrgb按钮产生的两个old
}

@property (nonatomic, strong) IBOutlet KNColorPlate *colorPlate;
@property (nonatomic, strong) IBOutlet UIImageView *bgImageView;
@property (nonatomic, strong) IBOutlet UIView *navView;
@property (nonatomic, strong) IBOutlet UIView *bottomView;
@property (nonatomic, strong) IBOutlet UIView *bottomBoundsView;
@property (nonatomic, strong) IBOutlet UIView *colorPlateContainer;
@property (nonatomic, strong) IBOutlet UIButton *onOffBtn;
@property (nonatomic, strong) IBOutlet UIButton *roomStateControlBtn;
@property (nonatomic, strong) IBOutlet UIScrollView *roomScrollView;
@property (nonatomic, strong) IBOutlet CBAutoScrollLabel *selectedRoomNameLab;
@property (nonatomic, strong) IBOutlet UIView *wBrightnessView;
@property (nonatomic, strong) IBOutlet UIView *rgbBrightnessView;
@property (nonatomic, strong) IBOutlet UIView *runView;
@property (nonatomic, strong) IBOutlet UILabel *wBrightnessLab;
@property (nonatomic, strong) IBOutlet UILabel *rgbBrightnessLab;
@property (nonatomic, strong) IBOutlet UILabel *runLab;
@property (nonatomic, strong) IBOutlet UIView *rgbSliderView;
@property (nonatomic, strong) IBOutlet UIButton *runBtn;
@property (nonatomic, strong) IBOutlet UIButton *smoothBtn;
@property (nonatomic, strong) IBOutlet UIButton *jumpsBtn;
@property (nonatomic, strong) IBOutlet UIButton *resetBtn;
@property (nonatomic, strong) IBOutlet UIButton *helpBtn;
@property (nonatomic, strong) IBOutlet UIButton *settingsBtn;

@property (nonatomic, strong) IBOutlet UIButton *numBtn1;
@property (nonatomic, strong) IBOutlet UIButton *numBtn2;
@property (nonatomic, strong) IBOutlet UIButton *numBtn3;
@property (nonatomic, strong) IBOutlet UIButton *numBtn4;
@property (nonatomic, strong) IBOutlet UIButton *numBtn5;
@property (nonatomic, strong) IBOutlet UIButton *toggleBtn;

@property (nonatomic, strong) KNTutorialViewController *tutorialViewController;
@property (nonatomic, strong) UIView *closeView;

@property (nonatomic, strong) NSArray *rooms;
@property (nonatomic, strong) WFRoom  *selectedRoom;
@property (nonatomic, strong) WFRoom  *editRoom;

@property (nonatomic, strong) NSTimer *sendWRGBTimer;
@property (nonatomic, strong) NSTimer *sendRGBTimer;
@property (nonatomic, strong) NSTimer *sendRunTimer;

@property (nonatomic, strong) NSArray *rgbCirlePositions;
@property (nonatomic, strong) NSArray *yellowCirlePositions;

@end

@implementation KNHomeViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (CGRectEqualToRect(_onOffBtnOrgFrame, CGRectZero)) {
        _onOffBtnOrgFrame = _onOffBtn.frame;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //self.bgImageView.image = [[UIImage imageNamed:@"bg_medium"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
    _roomType = kWFRoomTypeRGB;
    
    self.closeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 45)];
    self.closeView.backgroundColor = [UIColor blackColor];
    self.closeView.alpha = 0.8;
    _closeView.hidden = YES;
    
    [self.view insertSubview:_closeView belowSubview:_onOffBtn];
    if (kSystemVersion < 7.0) {
        _navView.frame = CGRectOffset(_navView.frame, 0, -20);
    }
    
    self.colorPlate.colorImage = [UIImage imageNamed:@"img_wheel"];
    self.colorPlate.selectorImage = [UIImage imageNamed:@"img_selector"];
    self.colorPlate.delegate = self;
    
    WFLoadingViewController *loadingViewController = [[WFLoadingViewController alloc] init];
    loadingViewController.delegate = self;
    KNAppDelegate *delegate = (KNAppDelegate *)[[UIApplication sharedApplication] delegate];
    loadingViewController.view.frame = delegate.window.bounds;
    [delegate.window.rootViewController.view addSubview:loadingViewController.view];
    
    self.rgbCirlePositions = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(20, 120)], [NSValue valueWithCGPoint:CGPointMake(41.5, 32.5)],[NSValue valueWithCGPoint:CGPointMake(128, 25)],[NSValue valueWithCGPoint:CGPointMake(166, 94.5)],[NSValue valueWithCGPoint:CGPointMake(104, 166)],nil];
    self.yellowCirlePositions = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(20, 120)], [NSValue valueWithCGPoint:CGPointMake(28, 45.5)],[NSValue valueWithCGPoint:CGPointMake(109.06621, 12.305214)],[NSValue valueWithCGPoint:CGPointMake(166, 94.5)],[NSValue valueWithCGPoint:CGPointMake(100.5, 165.5)],nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceScanDidCompleted:) name:kDeviceScanDidCompleted object:nil];
    
    _selectedRoomNameLab.font = [UIFont boldSystemFontOfSize:17.];
    _selectedRoomNameLab.textAlignment = NSTextAlignmentCenter;
    _selectedRoomNameLab.backgroundColor = [UIColor clearColor];
    _selectedRoomNameLab.textColor = [UIColor whiteColor];
    
    [self refreshNameLabel];
    
    if ([[UIDevice currentDevice] isDevice5]) {
        CGFloat statusReverse = 0;
        if (kSystemVersion < 7) {
            statusReverse = 20;
        }
        
        _colorPlateContainer.frame = CGRectMake(0, (CGRectGetHeight(self.view.bounds) - CGRectGetHeight(_navView.frame) - CGRectGetMidY(_bottomView.frame) - 180) / 2. - 20 - statusReverse, 320, 180);
        _rgbSliderView.frame = _colorPlateContainer.frame;
        
        _wBrightnessView.frame = CGRectMake(_wBrightnessView.frame.origin.x, 270 - statusReverse, _wBrightnessView.frame.size.width, _wBrightnessView.frame.size.height);
        _runView.frame = CGRectMake(_runView.frame.origin.x, 270 - statusReverse, _runView.frame.size.width, _runView.frame.size.height);
        _rgbBrightnessView.frame = CGRectMake(_rgbBrightnessView.frame.origin.x, 270 - statusReverse, _rgbBrightnessView.frame.size.width, _rgbBrightnessView.frame.size.height);;
        
        [_wBrightnessView removeFromSuperview];
        [_runView removeFromSuperview];
        [_rgbBrightnessView removeFromSuperview];
        
        [self.view insertSubview:_wBrightnessView belowSubview:_closeView];
        [self.view insertSubview:_runView belowSubview:_closeView];
        [self.view insertSubview:_rgbBrightnessView belowSubview:_closeView];
    } else {
        _colorPlateContainer.frame = CGRectMake(0, (CGRectGetHeight(self.view.bounds) - CGRectGetHeight(_navView.frame) - CGRectGetMidY(_bottomView.frame) - 180 - 20) / 2., 320, 180);
        _rgbSliderView.frame = _colorPlateContainer.frame;
        if (kSystemVersion >= 7) {
            _colorPlateContainer.frame = CGRectOffset(_colorPlateContainer.frame, 0, 10);
        }
        _colorPlateContainer.frame = CGRectOffset(_colorPlateContainer.frame, 0, -3);
    }
    
    _wBrightnessValue = kMaxWBrightnessValue;
    _rgbBrightnessValue = kMaxRGBBrightnessValue;
    
    _oldRgbBrightnessValue = _rgbBrightnessValue;
    _oldwRgbBrightnessValue = _wBrightnessValue;
    
    _runValue = kMinRunValue;
    
    _wBrightnessLab.text = [NSString stringWithFormat:@"%d", _wBrightnessValue];
    _rgbBrightnessLab.text = [NSString stringWithFormat:@"%d", _rgbBrightnessValue];
    _runLab.text = [NSString stringWithFormat:@"%d", _runValue / 60];
    
    [self refreshMidView];
    
    NSInteger capSize = 3;
    for (NSInteger i = 100; i < 103; i++) {
        UISlider *slider = (UISlider *)[_rgbSliderView viewWithTag:i];
        [slider setThumbImage:[UIImage imageNamed:@"img_handle"] forState:UIControlStateNormal];
        [slider setMinimumTrackImage:[[UIImage imageNamed:@"img_slider_fill"] stretchableImageWithLeftCapWidth:capSize topCapHeight:capSize] forState:UIControlStateNormal];
        [slider setMaximumTrackImage:[[UIImage imageNamed:@"img_slider"] stretchableImageWithLeftCapWidth:capSize topCapHeight:capSize] forState:UIControlStateNormal];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(roomDidDeleted:) name:kNotiRoomDidDeleted object:nil];
    
    if (_selectedRoom.run.boolValue) {
        _runBtn.selected = NO;
        [self runBtnDidPressed:_runBtn];
    }
    [self refreshMidView];
    
    [self initScroll];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSNotificationCenter

- (void)roomDidDeleted:(id)sender
{
    self.editRoom = nil;
    [self initScroll];
    [self refreshNameLabel];
}

- (void)deviceScanDidCompleted:(id)sender
{
    self.selectedRoom = nil;
    [self initScroll];
    [self refreshNameLabel];
}

#pragma mark - Private

- (void)refreshMidView
{
    static CGFloat space = 46.;
    NSInteger viewShowedCount = 2;
    if (_roomType == kWFRoomTypeRGB) {
        _rgbBrightnessView.hidden = NO;
        _wBrightnessView.hidden = YES;
        
        _colorPlate.is255Mode = NO;
        if (_runBtn.selected) {
            _resetBtn.hidden = NO;
            _jumpsBtn.hidden = NO;
            _smoothBtn.hidden = NO;
            
            _rgbSliderView.hidden = YES;
            _toggleBtn.selected = NO;
            _toggleBtn.hidden = YES;
            _colorPlateContainer.hidden = NO;
        } else {
            _resetBtn.hidden = YES;
            _jumpsBtn.hidden = YES;
            _smoothBtn.hidden = YES;
            _toggleBtn.hidden = NO;
        }
        _jumpsBtn.selected = _selectedRoom.jump.boolValue;
        _smoothBtn.selected = _selectedRoom.smooth.boolValue;
        
        CGFloat positionX = (CGRectGetWidth(self.view.frame) - CGRectGetWidth(_rgbBrightnessView.frame) - (viewShowedCount - 1) * space) / viewShowedCount - 24;
        if (_runBtn.selected) {
            _runView.hidden = NO;
        } else {
            _runView.hidden = YES;
            positionX = (CGRectGetWidth(self.view.frame) - CGRectGetWidth(_rgbBrightnessView.frame) - (viewShowedCount - 1) * space) / viewShowedCount + 24;
        }
        
        _rgbBrightnessView.frame = CGRectMake(positionX, _rgbBrightnessView.frame.origin.y, _rgbBrightnessView.frame.size.width, _rgbBrightnessView.frame.size.height);
        positionX += space + CGRectGetWidth(_rgbBrightnessView.frame);
        
        if (!_runView.hidden) {
            _runView.frame = CGRectMake(positionX, _runView.frame.origin.y, _runView.frame.size.width, _runView.frame.size.height);
        }
    } else if (_roomType == kWFRoomTypeRGBW) {
        _wBrightnessView.hidden = NO;
        _rgbBrightnessView.hidden = NO;
        _colorPlate.is255Mode = NO;
        
        if (_runBtn.selected) {
            _resetBtn.hidden = NO;
            _jumpsBtn.hidden = NO;
            _smoothBtn.hidden = NO;
            
            _rgbSliderView.hidden = YES;
            _toggleBtn.selected = NO;
            _toggleBtn.hidden = YES;
            _colorPlateContainer.hidden = NO;
        } else {
            _resetBtn.hidden = YES;
            _jumpsBtn.hidden = YES;
            _smoothBtn.hidden = YES;
            
            _toggleBtn.hidden = NO;
        }
        
        _jumpsBtn.selected = _selectedRoom.jump.boolValue;
        _smoothBtn.selected = _selectedRoom.smooth.boolValue;
        
        if (_toggleBtn.selected) {
            _rgbSliderView.hidden = NO;
        }
        if (_runBtn.selected) {
            _runView.hidden = NO;
            viewShowedCount++;
        } else {
            _runView.hidden = YES;
        }
        
        CGFloat positionX = (CGRectGetWidth(self.view.frame) - CGRectGetWidth(_wBrightnessView.frame) - (viewShowedCount - 1) * space) / viewShowedCount - 24;
        _wBrightnessView.frame = CGRectMake(positionX, _wBrightnessView.frame.origin.y, _wBrightnessView.frame.size.width, _wBrightnessView.frame.size.height);
        positionX += space + CGRectGetWidth(_wBrightnessView.frame);
        
        if (!_runView.hidden) {
            _runView.frame = CGRectMake(positionX, _runView.frame.origin.y, _runView.frame.size.width, _runView.frame.size.height);
            positionX += space + CGRectGetWidth(_runView.frame);
        }
        
        _rgbBrightnessView.frame = CGRectMake(positionX, _rgbBrightnessView.frame.origin.y, _rgbBrightnessView.frame.size.width, _rgbBrightnessView.frame.size.height);
        
    } else if (_roomType == kWFRoomTypeCWWW) {
        _rgbBrightnessView.hidden = NO;
        _wBrightnessView.hidden = YES;
        _runView.hidden = YES;
        _toggleBtn.hidden = YES;
        _toggleBtn.selected = NO;
        _rgbSliderView.hidden = YES;
        _colorPlateContainer.hidden = NO;
        _colorPlate.is255Mode = YES;
        if (_runBtn.selected) {
            _resetBtn.hidden = NO;
            _jumpsBtn.hidden = NO;
            _smoothBtn.hidden = NO;
        } else {
            _resetBtn.hidden = YES;
            _jumpsBtn.hidden = YES;
            _smoothBtn.hidden = YES;
        }
        
        _jumpsBtn.selected = _selectedRoom.jump.boolValue;
        _smoothBtn.selected = _selectedRoom.smooth.boolValue;
        
        CGFloat positionX = (CGRectGetWidth(self.view.frame) - CGRectGetWidth(_rgbBrightnessView.frame) - (viewShowedCount - 1) * space) / viewShowedCount - 24;
        if (_runBtn.selected) {
            _runView.hidden = NO;
        } else {
            _runView.hidden = YES;
            positionX = (CGRectGetWidth(self.view.frame) - CGRectGetWidth(_rgbBrightnessView.frame) - (viewShowedCount - 1) * space) / viewShowedCount + 24;
        }
        
        _rgbBrightnessView.frame = CGRectMake(positionX, _rgbBrightnessView.frame.origin.y, _rgbBrightnessView.frame.size.width, _rgbBrightnessView.frame.size.height);
        positionX += space + CGRectGetWidth(_rgbBrightnessView.frame);
        
        if (!_runView.hidden) {
            _runView.frame = CGRectMake(positionX, _runView.frame.origin.y, _runView.frame.size.width, _runView.frame.size.height);
        }
    }
}

- (KNAppDelegate *)appDelegate
{
    return (KNAppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (NSManagedObjectContext *)managedObjectContext
{
    KNAppDelegate *delegate = self.appDelegate;
    return delegate.managedObjectContext;
}

- (UIView *)createDividerViewWithPosition:(CGPoint)position
{
    UIView *dividerView = [[UIView alloc] initWithFrame:CGRectMake(position.x, position.y, 1, kDividerHeight)];
    dividerView.backgroundColor = kLightTextColor;
    return dividerView;
}

- (void)refreshNameLabel
{
    NSString *roomNameStr = @"";
    
    if (_allRoomSelected && _rooms.count > 1) {
        roomNameStr = @"ALL";
    } else {
        for (WFRoom *room in _rooms) {
            if (room.selected.boolValue) {
                roomNameStr = [roomNameStr stringByAppendingFormat:@"%@ ", room.name];
            }
        }
        if (roomNameStr.length == 0) {
            roomNameStr = @"Please select Zone";
        }
    }
    
    _selectedRoomNameLab.text = roomNameStr;
}

- (void)initScroll
{
    for (id subView in _roomScrollView.subviews) {
        [subView removeFromSuperview];
    }
    
    self.rooms = [NSArray arrayWithArray:[WFNetworkClient sharedNetworkClient].rooms];
    
    static CGFloat dividerSpace = 15.;
    CGFloat curPostionX = 0;
    UIView *dividerView = nil;
    UIButton *allBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    allBtn.tag = 1000;
    [allBtn addTarget:self action:@selector(allBtnDidPressed:) forControlEvents:UIControlEventTouchUpInside];
    allBtn.frame = CGRectMake(0, 0, 49, CGRectGetHeight(_roomScrollView.frame));
    [allBtn setTitle:@"ALL" forState:UIControlStateNormal];
    [allBtn setTitleColor:kLightTextColor forState:UIControlStateNormal];
    [allBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [_roomScrollView addSubview:allBtn];
    curPostionX += CGRectGetWidth(allBtn.frame) + dividerSpace;
    
    dividerView = [self createDividerViewWithPosition:CGPointMake(curPostionX, (CGRectGetHeight(_roomScrollView.frame) - kDividerHeight) / 2.)];
    [_roomScrollView addSubview:dividerView];
    curPostionX += CGRectGetWidth(dividerView.frame) + dividerSpace;
    
    _allRoomSelected = YES;
    WFRoomType roomType = kWFRoomTypeRGB;
    NSInteger selectedIndex = -1;
    for (WFRoom *room in _rooms) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = [_rooms indexOfObject:room] + 100;
        btn.frame = CGRectMake(curPostionX, 0, 55, 55);
        [btn setImage:[UIImage imageWithData:room.thumbnails] forState:UIControlStateNormal];
        [_roomScrollView addSubview:btn];
        
        [btn addTarget:self action:@selector(roomBtnDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        UILongPressGestureRecognizer *lpg = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(roomBtnDidLongPressed:)];
        [btn addGestureRecognizer:lpg];
        
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(roomDoubleTap:)];
        [doubleTap setNumberOfTapsRequired:2];
        [btn addGestureRecognizer:doubleTap];
        
        curPostionX += CGRectGetWidth(btn.frame) + dividerSpace;
        
        UIFont *font = [UIFont systemFontOfSize:16.];
        CGSize size = [room.name sizeWithAttributes:@{NSFontAttributeName:font}];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(curPostionX, (CGRectGetHeight(_roomScrollView.frame) - size.height) / 2., size.width, size.height)];
        label.userInteractionEnabled = YES;
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(roomLableTap:)];
        [label addGestureRecognizer:ges];
        label.tag = 100 + btn.tag;
        label.font = font;
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        label.text = room.name;
        [_roomScrollView addSubview:label];
        curPostionX += CGRectGetWidth(label.frame) + dividerSpace;
        
        dividerView = [self createDividerViewWithPosition:CGPointMake(curPostionX, (CGRectGetHeight(_roomScrollView.frame) - kDividerHeight) / 2.)];
        [_roomScrollView addSubview:dividerView];
        curPostionX += CGRectGetWidth(dividerView.frame) + dividerSpace;
        
        if (room.selected.boolValue) {
            self.selectedRoom = room;
            roomType = room.type.integerValue;
            selectedIndex = btn.tag;
        } else {
            _allRoomSelected = NO;
            label.textColor = [UIColor lightGrayColor];
        }
    }
    if (_selectedRoom) {
        _closeView.hidden = _selectedRoom.actived.boolValue;
        if (!_selectedRoom.actived.boolValue) {
            _onOffBtn.selected = YES;
        }
    }
    
    if (roomType == kWFRoomTypeCWWW) {
        _colorPlate.colorImage = [UIImage imageNamed:@"img_wheel_cwww"];
        [_colorPlate setCirclePointPositions:_yellowCirlePositions];
    } else {
        _colorPlate.colorImage = [UIImage imageNamed:@"img_wheel"];
        [_colorPlate setCirclePointPositions:_rgbCirlePositions];
    }
    if (_roomType == kWFRoomTypeDefault) {
        _roomType = kWFRoomTypeRGB;
    }
    _roomType = roomType;
    
//    UIButton *addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [addBtn addTarget:self action:@selector(addBtnDidPressed:) forControlEvents:UIControlEventTouchUpInside];
//    addBtn.frame = CGRectMake(curPostionX, (CGRectGetHeight(_roomScrollView.frame) - 49) / 2., 49, 49);
//    [addBtn setImage:[UIImage imageNamed:@"btn_add"] forState:UIControlStateNormal];
//    [addBtn setImage:[UIImage imageNamed:@"btn_add_click"] forState:UIControlStateHighlighted];
//    [_roomScrollView addSubview:addBtn];
//    curPostionX += CGRectGetWidth(addBtn.frame);
    
    _roomScrollView.contentSize = CGSizeMake(curPostionX, 0);
    if (_allRoomSelected && _rooms.count > 1) {
        [allBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        for (WFRoom *room in _rooms) {
            UIButton *btn = (UIButton *)[_roomScrollView viewWithTag:[_rooms indexOfObject:room] + 100];
            UILabel *lab = (UILabel *)[_roomScrollView viewWithTag:100 + btn.tag];
            lab.textColor = [UIColor lightGrayColor];
        }
    } else {
        if (selectedIndex >= 0) {
            UIButton *btn = (UIButton *)[_roomScrollView viewWithTag:selectedIndex];
            UILabel *lab = (UILabel *)[_roomScrollView viewWithTag:100 + btn.tag];
            [_roomScrollView setContentOffset:CGPointMake(CGRectGetMinX(btn.frame) + CGRectGetWidth(btn.frame) / 2. + CGRectGetWidth(lab.frame) / 2. + 15. / 2. - CGRectGetWidth(self.view.frame) / 2., 0) animated:YES];
        }
    }
}

- (void)sendData:(WFNetworkData *)networkData
{
    WFNetworkClient *networkClient = [WFNetworkClient sharedNetworkClient];
    [networkClient sendNetworkData:networkData];
}

- (void)resetNumBtnState
{
    _numBtn1.selected = NO;
    _numBtn2.selected = NO;
    _numBtn3.selected = NO;
    _numBtn4.selected = NO;
    _numBtn5.selected = NO;
}

- (void)sendWBrightnessWithState:(BOOL)isUp
{
    if (isUp) {
        if (_wBrightnessValue < _rgbBrightnessValue) {
            _wBrightnessValue++;
        }
        _wBrightnessLab.text = [NSString stringWithFormat:@"%d", _wBrightnessValue];
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        networkData.opcode = '\x05';
        networkData.opvalue1 = _wBrightnessValue;
        networkData.opvalue2 = '\x00';
        networkData.opvalue3 = '\x00';
        [self sendData:networkData];
    } else {
        if (_wBrightnessValue > kMinWBrightnessValue) {
            _wBrightnessValue--;
        }
        _wBrightnessLab.text = [NSString stringWithFormat:@"%d", _wBrightnessValue];
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        networkData.opcode = '\x05';
        networkData.opvalue1 = _wBrightnessValue;
        networkData.opvalue2 = '\x00';
        networkData.opvalue3 = '\x00';
        [self sendData:networkData];
    }
    
    if (!isUp && _wBrightnessValue == 0) {
        _wRGBOperateToZearo = YES;
    } else {
        _wRGBOperateToZearo = NO;
    }
    _oldwRgbBrightnessValue = _wBrightnessValue;
    _oldRgbBrightnessValue = _rgbBrightnessValue;
}

- (void)sendRgbBrightnessWithState:(BOOL)isUp
{
    if (_wBrightnessValue != 0) {
        if (isUp && _rgbBrightnessValue < kMaxRGBBrightnessValue) {
            _wBrightnessValue++;
        } else if (!isUp && _rgbBrightnessValue > kMinRGBBrightnessValue && _wBrightnessValue > 1) {
            _wBrightnessValue--;
        }
    }
    _wBrightnessLab.text = [NSString stringWithFormat:@"%d", _wBrightnessValue];
    

    if (isUp) {
        if (_rgbBrightnessValue < kMaxRGBBrightnessValue) {
            _rgbBrightnessValue++;
        }
        _rgbBrightnessLab.text = [NSString stringWithFormat:@"%d", _rgbBrightnessValue];
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        if (_roomType == kWFRoomTypeCWWW) {
            networkData.opcode = '\x34';
        } else {
            networkData.opcode = '\x07';
        }
        networkData.opvalue1 = _rgbBrightnessValue;
        networkData.opvalue2 = '\x00';
        networkData.opvalue3 = '\x00';
        [self sendData:networkData];
    } else {
        if (_rgbBrightnessValue > kMinRGBBrightnessValue) {
            _rgbBrightnessValue--;
        }
        _rgbBrightnessLab.text = [NSString stringWithFormat:@"%d", _rgbBrightnessValue];
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        if (_roomType == kWFRoomTypeCWWW) {
            networkData.opcode = '\x34';
        } else {
            networkData.opcode = '\x07';
        }
        networkData.opvalue1 = _rgbBrightnessValue;
        networkData.opvalue2 = '\x00';
        networkData.opvalue3 = '\x00';
        [self sendData:networkData];
    }

//    if (_isRrriveLimited) {
//        if (_wBrightnessValue <= _oldwRgbBrightnessValue && _rgbBrightnessValue <= _oldRgbBrightnessValue) {
//            _isRrriveLimited = NO;
//        }
//        if (!_wRGBOperateToZearo) {
//            _wBrightnessValue = (CGFloat)(_rgbBrightnessValue - _oldRgbBrightnessValue) / 2. + _oldCurWRgbBrightnessValue;
//        }
//    } else {
//        _wBrightnessValue = ((CGFloat) _rgbBrightnessValue / (CGFloat)_oldRgbBrightnessValue) * _oldwRgbBrightnessValue;
//        if (_wBrightnessValue >= _oldwRgbBrightnessValue && _rgbBrightnessValue >= _oldRgbBrightnessValue) {
//            _isRrriveLimited = YES;
//            _oldCurWRgbBrightnessValue = _wBrightnessValue;
//        }
//    }
//    
//    if (!isUp && !_wRGBOperateToZearo && _rgbBrightnessValue != 0 && _wBrightnessValue == 0) {
//        _wBrightnessValue = 1;
//    }
//    
//    _wBrightnessLab.text = [NSString stringWithFormat:@"%d", _wBrightnessValue];
}

- (void)sendRunWithState:(BOOL)isUp
{
    if (isUp) {
        if (_runValue < kMaxRunValue) {
            _runValue += 60;
        }
        _runLab.text = [NSString stringWithFormat:@"%d", _runValue / 60];
        int val = _runValue;
        char *bytes = (char*) &val;
        
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        networkData.opcode = '\x32';
        networkData.opvalue1 = '\x00';
        networkData.opvalue2 = bytes[1];
        networkData.opvalue3 = bytes[0];
        [self sendData:networkData];
    } else {
        if (_runValue > kMinRunValue) {
            _runValue -= 60;
        }
        _runLab.text = [NSString stringWithFormat:@"%d", _runValue / 60];
        int val = _runValue;
        char *bytes = (char*) &val;
        
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        networkData.opcode = '\x33';
        networkData.opvalue1 = '\x00';
        networkData.opvalue2 = bytes[1];
        networkData.opvalue3 = bytes[0];
        [self sendData:networkData];
    }
}

#pragma mark - Action

- (void)addBtnDidPressed:(id)sender
{
    if (!_rooms || _rooms.count < 16) {
        [self performSegueWithIdentifier:@"toKNAddRoomViewController" sender:nil];
    }
}

- (void)allBtnDidPressed:(id)sender
{
    if (_rooms.count == 0) {
        return;
    }
    
    BOOL hasDifType = NO;
    WFRoom *fRoom = [_rooms objectAtIndex:0];
    WFRoomType roomType = fRoom.type.integerValue;
    
    for (WFRoom *room in _rooms) {
        if (roomType != room.type.integerValue) {
            hasDifType = YES;
            break;
        }
    }
    if (hasDifType) {
        return;
    }
    
    for (WFRoom *room in _rooms) {
        room.selected = [NSNumber numberWithBool:YES];
        [[WFNetworkData shareNetworkData] selectSubDeviceNumber:[NSString stringWithFormat:@"%d", [_rooms indexOfObject:room] + 1]];
    }
    //[self.appDelegate saveContext];
    
    UIButton *lastSelectedBtn = (UIButton *)[_roomScrollView viewWithTag:[_rooms indexOfObject:_selectedRoom] + 100];
    if (lastSelectedBtn) {
        UILabel *lastSelectedLab = (UILabel *)[_roomScrollView viewWithTag:100 + lastSelectedBtn.tag];
        lastSelectedLab.textColor = [UIColor lightGrayColor];
    }
    
    UIButton *allBtn = (UIButton *)[_roomScrollView viewWithTag:1000];
    [allBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.selectedRoom = [_rooms objectAtIndex:0];
    _allRoomSelected = YES;
    
    if (roomType == kWFRoomTypeRGB || roomType == kWFRoomTypeRGBW) {
        _colorPlate.colorImage = [UIImage imageNamed:@"img_wheel"];
    } else {
        _colorPlate.colorImage = [UIImage imageNamed:@"img_wheel_cwww"];
    }
    [self refreshNameLabel];
}

- (IBAction)onOffBtnDidPressed:(UIButton *)btn
{
    btn.selected = !btn.selected;
    btn.enabled = NO;
    [UIView animateWithDuration:1. animations:^{
        _closeView.hidden = !btn.selected;
    } completion:^(BOOL finished) {
        btn.enabled = YES;
    }];
    if (btn.selected) {
        if (_allRoomSelected) {
            for (WFRoom *room in _rooms) {
                room.actived = [NSNumber numberWithBool:NO];
            }
        } else {
            _selectedRoom.actived = [NSNumber numberWithBool:NO];
        }
        
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        networkData.opcode = '\x01';
        networkData.opvalue1 = '\x00';
        networkData.opvalue2 = '\x00';
        networkData.opvalue3 = '\x00';
        [self sendData:networkData];
    } else {
        if (_allRoomSelected) {
            for (WFRoom *room in _rooms) {
                room.actived = [NSNumber numberWithBool:YES];
            }
        } else {
            _selectedRoom.actived = [NSNumber numberWithBool:YES];
        }
        
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        networkData.opcode = '\x02';
        networkData.opvalue1 = '\x00';
        networkData.opvalue2 = '\x00';
        networkData.opvalue3 = '\x00';
        [self sendData:networkData];
    }
    //[self.appDelegate saveContext];
}

- (IBAction)roomStateControlBtnDidPressed:(UIButton *)btn
{
    _roomShowed = !_roomShowed;
    btn.enabled = NO;
    [UIView animateWithDuration:.5 animations:^{
        if (_roomShowed) {
            _bottomView.frame = CGRectMake(_bottomView.frame.origin.x, -72, _bottomView.frame.size.width, _bottomView.frame.size.height);
            btn.transform = CGAffineTransformMakeRotation(-M_PI);
            _onOffBtn.frame = CGRectMake(_onOffBtnOrgFrame.origin.x, _onOffBtnOrgFrame.origin.y - 72, _onOffBtnOrgFrame.size.width, _onOffBtnOrgFrame.size.height);
            _closeView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 45 - 72);
        } else {
            _bottomView.frame = CGRectMake(_bottomView.frame.origin.x, 0, _bottomView.frame.size.width, _bottomView.frame.size.height);
            btn.transform = CGAffineTransformMakeRotation(0);
            _onOffBtn.frame = _onOffBtnOrgFrame;
            _closeView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 45);
        }
    } completion:^(BOOL finished) {
        btn.enabled = YES;
    }];
}

- (void)roomBtnDidLongPressed:(UILongPressGestureRecognizer *)lpg
{
    if (lpg.state == UIGestureRecognizerStateBegan) {
        UIButton *btn = (UIButton *)lpg.view;
        WFRoom *room = [_rooms objectAtIndex:btn.tag - 100];
        self.editRoom = room;
//        [self performSegueWithIdentifier:@"toKNEditRoomViewController" sender:nil];
        
        KNEditRoomViewController *editRoomViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"KNEditRoomViewController"];
        editRoomViewController.delegate = self;
        editRoomViewController.room = room;
        [self.navigationController pushViewController:editRoomViewController animated:YES];
    }
}

- (void)roomLableTap:(UIGestureRecognizer *)ges
{
    UILabel *roomLabel = (UILabel *)ges.view;
    UIButton *btn = (UIButton *)[_roomScrollView viewWithTag:roomLabel.tag - 100];
    [self roomBtnDidPressed:btn];
}

- (void)roomDoubleTap:(UITapGestureRecognizer *)tap
{
    
}

- (IBAction)smoothBtnDidPressed:(UIButton *)smoothBtn
{
    smoothBtn.selected = YES;
    _jumpsBtn.selected = NO;
    
    if (_allRoomSelected) {
        for (WFRoom *room in _rooms) {
            room.smooth = [NSNumber numberWithBool:smoothBtn.selected];
            room.jump = [NSNumber numberWithBool:NO];
        }
    } else {
        _selectedRoom.smooth = [NSNumber numberWithBool:smoothBtn.selected];
        _selectedRoom.jump = [NSNumber numberWithBool:NO];
    }
    
    WFNetworkData *networkData = [WFNetworkData shareNetworkData];
    networkData.opcode = '\x31';
    networkData.opvalue1 = '\x01';
    networkData.opvalue2 = '\x00';
    networkData.opvalue3 = '\x00';
    [self sendData:networkData];
}

- (IBAction)runBtnDidPressed:(UIButton *)runBtn
{
    [self resetNumBtnState];
    runBtn.selected = !runBtn.selected;
    _colorPlate.isCirclePointsMode = runBtn.selected;
    if (runBtn.selected) {
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        networkData.opcode = '\x13';
        networkData.opvalue1 = '\x01';
        networkData.opvalue2 = '\x00';
        networkData.opvalue3 = '\x00';
        [self sendData:networkData];
        
        if (_selectedRoom.smooth.boolValue) {
            [self smoothBtnDidPressed:_smoothBtn];
        } else if (_selectedRoom.jump.boolValue) {
            [self jumpsBtnDidPressed:_jumpsBtn];
        }
    } else {
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        networkData.opcode = '\x13';
        networkData.opvalue1 = '\x02';
        networkData.opvalue2 = '\x00';
        networkData.opvalue3 = '\x00';
        [self sendData:networkData];
    }
    [self refreshMidView];
    
    if (_allRoomSelected) {
        for (WFRoom *room in _rooms) {
            room.run = [NSNumber numberWithBool:runBtn.selected];
        }
    } else {
        _selectedRoom.run = [NSNumber numberWithBool:runBtn.selected];
    }
}

- (IBAction)runBtnDidLongPressed:(UILongPressGestureRecognizer *)lpg
{
    if (lpg.state == UIGestureRecognizerStateBegan) {
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        networkData.opcode = '\x2E';
        networkData.opvalue1 = '\x01';
        networkData.opvalue2 = '\x00';
        networkData.opvalue3 = '\x00';
        [self sendData:networkData];
    }
}

- (IBAction)resetBtnDidPressed:(UIButton *)btn
{
    switch (_roomType) {
        case kWFRoomTypeCWWW:
            self.yellowCirlePositions = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(20, 120)], [NSValue valueWithCGPoint:CGPointMake(28, 45.5)],[NSValue valueWithCGPoint:CGPointMake(109.06621, 12.305214)],[NSValue valueWithCGPoint:CGPointMake(166, 94.5)],[NSValue valueWithCGPoint:CGPointMake(100.5, 165.5)],nil];
            [_colorPlate setCirclePointPositions:_yellowCirlePositions];
            break;
        default:
            self.rgbCirlePositions = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(20, 120)], [NSValue valueWithCGPoint:CGPointMake(41.5, 32.5)],[NSValue valueWithCGPoint:CGPointMake(128, 25)],[NSValue valueWithCGPoint:CGPointMake(166, 94.5)],[NSValue valueWithCGPoint:CGPointMake(104, 166)],nil];
            [_colorPlate setCirclePointPositions:_rgbCirlePositions];
            break;
    }
    
    WFNetworkData *networkData = [WFNetworkData shareNetworkData];
    networkData.opcode = '\x30';
    networkData.opvalue1 = '\x01';
    networkData.opvalue2 = '\x00';
    networkData.opvalue3 = '\x00';
    [self sendData:networkData];
}

- (IBAction)jumpsBtnDidPressed:(UIButton *)btn
{
    btn.selected = YES;
    _smoothBtn.selected = NO;
    
    if (_allRoomSelected) {
        for (WFRoom *room in _rooms) {
            room.jump = [NSNumber numberWithBool:btn.selected];
            room.smooth = [NSNumber numberWithBool:NO];
        }
    } else {
        _selectedRoom.jump = [NSNumber numberWithBool:btn.selected];
        _selectedRoom.smooth = [NSNumber numberWithBool:NO];
    }
    
    WFNetworkData *networkData = [WFNetworkData shareNetworkData];
    networkData.opcode = '\x31';
    networkData.opvalue1 = '\x02';
    networkData.opvalue2 = '\x00';
    networkData.opvalue3 = '\x00';
    [self sendData:networkData];
}

- (void)roomBtnDidPressed:(UIButton *)btn
{
    WFRoom *OperRoom = [_rooms objectAtIndex:btn.tag - 100];
    OperRoom.selected = [NSNumber numberWithBool:YES];
    for (WFRoom *room in _rooms) {
        if (![OperRoom isEqual:room]) {
            room.selected = [NSNumber numberWithBool:NO];
        }
    }
    //[self.appDelegate saveContext];
    
    if (_selectedRoom) {
        UIButton *lastSelectedBtn = (UIButton *)[_roomScrollView viewWithTag:[_rooms indexOfObject:_selectedRoom] + 100];
        UILabel *lastSelectedLab = (UILabel *)[_roomScrollView viewWithTag:100 + lastSelectedBtn.tag];
        if (lastSelectedBtn && lastSelectedLab && [lastSelectedLab isKindOfClass:[UILabel class]]) {
            lastSelectedLab.textColor = [UIColor lightGrayColor];
        }
    }
    
    self.selectedRoom = OperRoom;
    _allRoomSelected = NO;
    _roomType = OperRoom.type.integerValue;
    
    _runBtn.selected = _selectedRoom.run.boolValue;
    _colorPlate.isCirclePointsMode = _runBtn.selected;
    
    [[WFNetworkData shareNetworkData] resetSubDeviceNumberStatus];
    [[WFNetworkData shareNetworkData] selectSubDeviceNumber:[NSString stringWithFormat:@"%d", btn.tag - 100 + 1]];
    
    if (OperRoom.type.integerValue == kWFRoomTypeRGB || OperRoom.type.integerValue == kWFRoomTypeRGBW) {
        _colorPlate.colorImage = [UIImage imageNamed:@"img_wheel"];
        [_colorPlate setCirclePointPositions:_rgbCirlePositions];
    } else {
        _colorPlate.colorImage = [UIImage imageNamed:@"img_wheel_cwww"];
        [_colorPlate setCirclePointPositions:_yellowCirlePositions];
    }
    [self refreshNameLabel];
    [self refreshMidView];
    _closeView.hidden = OperRoom.actived.boolValue;
    _onOffBtn.selected = !_selectedRoom.actived.boolValue;
    
    UILabel *lab = (UILabel *)[_roomScrollView viewWithTag:100 + btn.tag];
    lab.textColor = [UIColor whiteColor];
    [_roomScrollView setContentOffset:CGPointMake(CGRectGetMinX(btn.frame) + CGRectGetWidth(btn.frame) / 2. + CGRectGetWidth(lab.frame) / 2. + 15. / 2. - CGRectGetWidth(self.view.frame) / 2., 0) animated:YES];
    
    UIButton *allBtn = (UIButton *)[_roomScrollView viewWithTag:1000];
    [allBtn setTitleColor:kLightTextColor forState:UIControlStateNormal];    
}

- (IBAction)wBrightnessUpBtnDidPressed:(UIButton *)btn
{
    [self sendWBrightnessWithState:YES];
}

- (IBAction)wBrightnessDownBtnDidPressed:(UIButton *)btn
{
    [self sendWBrightnessWithState:NO];
}

- (IBAction)runUpBtnDidPressed:(UIButton *)btn
{
    [self sendRunWithState:YES];
}

- (IBAction)runDownBtnDidPressed:(UIButton *)btn
{
    [self sendRunWithState:NO];
}

- (IBAction)rgbBrightnessUpBtnDidPressed:(UIButton *)btn
{
    [self sendRgbBrightnessWithState:YES];
}

- (IBAction)rgbBrightnessDownBtnDidPressed:(UIButton *)btn
{
    [self sendRgbBrightnessWithState:NO];
}

- (IBAction)wBrightnessUpLongTap:(UIGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateBegan) {
        self.sendWRGBTimer = [NSTimer scheduledTimerWithTimeInterval:20. / 1000. target:self selector:@selector(sendWBrightnessTimer:) userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:kIsUp] repeats:YES];
    } else if (tap.state == UIGestureRecognizerStateEnded) {
        [_sendWRGBTimer invalidate];
        self.sendWRGBTimer = nil;
    }
}

- (IBAction)wBrightnessDownLongTap:(UIGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateBegan) {
        self.sendWRGBTimer = [NSTimer scheduledTimerWithTimeInterval:20. / 1000. target:self selector:@selector(sendWBrightnessTimer:) userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:kIsUp] repeats:YES];
    } else if (tap.state == UIGestureRecognizerStateEnded) {
        [_sendWRGBTimer invalidate];
        self.sendWRGBTimer = nil;
    }
}

- (IBAction)rgbBrightnessUpLongTap:(UIGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateBegan) {
        self.sendRGBTimer = [NSTimer scheduledTimerWithTimeInterval:20. / 1000. target:self selector:@selector(sendRgbBrightnessTimer:) userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:kIsUp] repeats:YES];
    } else if (tap.state == UIGestureRecognizerStateEnded) {
        [_sendRGBTimer invalidate];
        self.sendRGBTimer = nil;
    }
}

- (IBAction)rgbBrightnessDownLongTap:(UIGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateBegan) {
        self.sendRGBTimer = [NSTimer scheduledTimerWithTimeInterval:20. / 1000. target:self selector:@selector(sendRgbBrightnessTimer:) userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:kIsUp] repeats:YES];
    } else if (tap.state == UIGestureRecognizerStateEnded) {
        [_sendRGBTimer invalidate];
        self.sendRGBTimer = nil;
    }
}

- (IBAction)runUpLongTap:(UIGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateBegan) {
        self.sendRunTimer = [NSTimer scheduledTimerWithTimeInterval:20. / 1000. target:self selector:@selector(sendRunTimer:) userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:kIsUp] repeats:YES];
    } else if (tap.state == UIGestureRecognizerStateEnded) {
        [_sendRunTimer invalidate];
        self.sendRunTimer = nil;
    }
}

- (IBAction)runDownLongTap:(UIGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateBegan) {
        self.sendRunTimer = [NSTimer scheduledTimerWithTimeInterval:20. / 1000. target:self selector:@selector(sendRunTimer:) userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:kIsUp] repeats:YES];
    } else if (tap.state == UIGestureRecognizerStateEnded) {
        [_sendRunTimer invalidate];
        self.sendRunTimer = nil;
    }
}

- (IBAction)numBtnDidPressed:(UIButton *)btn
{
    [self resetNumBtnState];
    _runBtn.selected = NO;
    _resetBtn.hidden = YES;
    _smoothBtn.hidden = YES;
    _jumpsBtn.hidden = YES;
    [self refreshMidView];
    
    if (_allRoomSelected) {
        for (WFRoom *room in _rooms) {
            room.run = [NSNumber numberWithBool:NO];
        }
    } else {
        _selectedRoom.run = [NSNumber numberWithBool:NO];
    }
    
    _colorPlate.isCirclePointsMode = NO;
    btn.selected = !btn.selected;
    
    WFNetworkData *networkData = [WFNetworkData shareNetworkData];
    networkData.opcode = '\x09' + btn.tag;
    networkData.opvalue1 = '\x01';
    networkData.opvalue2 = '\x00';
    networkData.opvalue3 = '\x00';
    [self sendData:networkData];
}

- (IBAction)numBtnDidLongPressed:(UILongPressGestureRecognizer *)lpg
{
    if (lpg.state == UIGestureRecognizerStateBegan) {
        UIButton *btn = (UIButton *)lpg.view;
        [self resetNumBtnState];
        btn.selected = YES;
        _runBtn.selected = NO;
        _resetBtn.hidden = YES;
        _smoothBtn.hidden = YES;
        _jumpsBtn.hidden = YES;
        [self refreshMidView];
        _colorPlate.isCirclePointsMode = NO;
        
        WFNetworkData *networkData = [WFNetworkData shareNetworkData];
        networkData.opcode = '\x09' + btn.tag;
        networkData.opvalue1 = '\x02';
        networkData.opvalue2 = '\x00';
        networkData.opvalue3 = '\x00';
        [self sendData:networkData];
    }
}

- (IBAction)sliderBtnDidPressed:(UIButton *)btn
{
    btn.selected = !btn.selected;
    _rgbSliderView.hidden = !_rgbSliderView.hidden;
    _colorPlateContainer.hidden = !_rgbSliderView.hidden;
}

- (IBAction)sliderValueDidChanged:(UISlider *)slider
{
    [self resetNumBtnState];
    
    NSInteger tag = slider.tag - 100;
    WFNetworkData *networkData = [WFNetworkData shareNetworkData];
    networkData.opcode = '\x10' + tag;
    networkData.opvalue1 = slider.value;
    networkData.opvalue2 = '\x00';
    networkData.opvalue3 = '\x00';
    [self sendData:networkData];
}

#pragma mark - Timer

- (void)sendWBrightnessTimer:(NSTimer *)timer
{
    NSDictionary *userInfo = timer.userInfo;
    [self sendWBrightnessWithState:[[userInfo objectForKey:kIsUp] boolValue]];
}

- (void)sendRgbBrightnessTimer:(NSTimer *)timer
{
    NSDictionary *userInfo = timer.userInfo;
    [self sendRgbBrightnessWithState:[[userInfo objectForKey:kIsUp] boolValue]];
}

- (void)sendRunTimer:(NSTimer *)timer
{
    NSDictionary *userInfo = timer.userInfo;
    [self sendRunWithState:[[userInfo objectForKey:kIsUp] boolValue]];
}

#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //看答案
    if ([segue.identifier isEqualToString:@"toKNAddRoomViewController"]) {
        KNAddRoomViewController *addRoomViewController = segue.destinationViewController;
        addRoomViewController.delegate = self;
    } else if ([segue.identifier isEqualToString:@"toKNEditRoomViewController"]) {
        KNEditRoomViewController *editRoomViewController = segue.destinationViewController;
        editRoomViewController.room = _selectedRoom;
    }
}

#pragma mark - WFLoadingViewControllerDelegate

- (void)loadingViewControllerDidTimeOver:(WFLoadingViewController *)loadingViewController
{
    [loadingViewController.view removeFromSuperview];
    NSNumber *firstNum = [[NSUserDefaults standardUserDefaults] objectForKey:kFirstLoginKey];
    if (!firstNum) {
        KNTutorialViewController *tutorialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"KNTutorialViewController"];
        tutorialViewController.delegate =self;
        tutorialViewController.view.frame = self.view.bounds;
        [self.view addSubview:tutorialViewController.view];
        self.tutorialViewController = tutorialViewController;
    }
}

#pragma mark - KNTutorialViewControllerDelegate

- (void)tutorialViewControllerDidClose:(KNTutorialViewController *)tutorialViewController
{
    //[tutorialViewController.view removeFromSuperview];
    tutorialViewController.view.hidden = YES;
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:kFirstLoginKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - KNAddRoomViewControllerDelegate

- (void)addRoomViewController:(KNAddRoomViewController *)addRoomViewController didFinishWithImage:(UIImage *)image
{
    KNEditRoomViewController *editRoomViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"KNEditRoomViewController"];
    editRoomViewController.delegate = self;
    editRoomViewController.image = image;
    [addRoomViewController.cropController.navigationController pushViewController:editRoomViewController animated:YES];
}

#pragma mark - KNEditRoomViewControllerDelegate

- (void)editRoomViewControllerDidCancel:(KNEditRoomViewController *)editRoomViewController
{
    //[self initImagePickerController];
}

- (void)editRoomViewControllerDidSave:(KNEditRoomViewController *)editRoomViewController
{
    [self initScroll];
    [self refreshMidView];
    [self refreshNameLabel];
}

#pragma mark - KNColorPlateDelegate

- (void)colorPlate:(KNColorPlate *)colorPlate didSelectorUpdateRed:(int)red green:(int)green blue:(int)blue
{
    if (red >= 253 && green <= 20 && blue <= 20) {
        red = 255;
        green = 0;
        blue = 0;
    } else if (green >= 253 && red <= 20 && blue <= 20) {
        green = 255;
        red = 0;
        blue = 0;
    } else if (blue >= 253 && green <= 20 && red <= 20) {
        blue = 255;
        green = 0;
        red = 0;
    }
    
    NSLog(@"r = %d, g = %d, b = %d", red, green, blue);
    [self resetNumBtnState];
    
    WFNetworkData *networkData = [WFNetworkData shareNetworkData];
    if (_roomType == kWFRoomTypeCWWW) {
        networkData.opcode = '\x35';
    } else {
        networkData.opcode = '\x0F';
    }
    networkData.opvalue1 = red;
    networkData.opvalue2 = green;
    networkData.opvalue3 = blue;
    [self sendData:networkData];
}

- (void)colorPlate:(KNColorPlate *)colorPlate didCircle:(NSInteger)circleIndex updateRed:(int)red green:(int)green blue:(int)blue
{
    //NSLog(@"r = %d, g = %d, b = %d", red, green, blue);
    WFNetworkData *networkData = [WFNetworkData shareNetworkData];
    switch (circleIndex) {
        case 0:
            networkData.opcode = '\x2F';
            break;
        case 1:
            networkData.opcode = '\x14';
            break;
        case 2:
            networkData.opcode = '\x15';
            break;
        case 3:
            networkData.opcode = '\x16';
            break;
        case 4:
            networkData.opcode = '\x17';
            break;
        default:
            break;
    }
    networkData.opvalue1 = red;
    networkData.opvalue2 = green;
    networkData.opvalue3 = blue;
    [self sendData:networkData];
    
    for (NSInteger i = 100; i < 103; i++) {
        UISlider *slider = (UISlider *)[_rgbSliderView viewWithTag:i];
        switch (i) {
            case 100:
                slider.value = red;
                break;
            case 101:
                slider.value = green;
                break;
            case 102:
                slider.value = blue;
                break;
            default:
                break;
        }
    }
    
    switch (_roomType) {
        case kWFRoomTypeCWWW:
            self.yellowCirlePositions = _colorPlate.circlePoints;
            break;
        default:
            self.rgbCirlePositions = _colorPlate.circlePoints;
            break;
    }
}

- (void)colorPlate:(KNColorPlate *)colorPlate didCircle:(NSInteger)circleIndex updateValue:(NSInteger)cwwwValue
{
    [self resetNumBtnState];
    
    WFNetworkData *networkData = [WFNetworkData shareNetworkData];
    if (_colorPlate.isCirclePointsMode) {
        switch (circleIndex) {
            case 0:
                networkData.opcode = '\x2F';
                break;
            case 1:
                networkData.opcode = '\x14';
                break;
            case 2:
                networkData.opcode = '\x15';
                break;
            case 3:
                networkData.opcode = '\x16';
                break;
            case 4:
                networkData.opcode = '\x17';
                break;
            default:
                break;
        }
    } else {
        networkData.opcode = '\x35';
    }
    
    networkData.opvalue1 = cwwwValue;
    networkData.opvalue2 = 255 - cwwwValue;
    networkData.opvalue3 = '\x00';
    [self sendData:networkData];
}

@end
