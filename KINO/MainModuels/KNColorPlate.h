//
//  KNColorPlate.h
//  KINO
//
//  Created by Aidian.Tang on 13-12-7.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KNColorPlate;

@protocol KNColorPlateDelegate <NSObject>

- (void)colorPlate:(KNColorPlate *)colorPlate didSelectorUpdateRed:(int)red green:(int)green blue:(int)blue;
- (void)colorPlate:(KNColorPlate *)colorPlate didCircle:(NSInteger)circleIndex updateRed:(int)red green:(int)green blue:(int)blue;
- (void)colorPlate:(KNColorPlate *)colorPlate didCircle:(NSInteger)circleIndex updateValue:(NSInteger)cwwwValue;

@end

@interface KNColorPlate : UIView

@property (nonatomic, weak) id<KNColorPlateDelegate> delegate;
@property (nonatomic, assign) BOOL isCirclePointsMode;
@property (nonatomic, assign) BOOL is255Mode; //整个圆环上下分割255份，也就是白色彩环类型
@property (nonatomic, strong) NSArray *circlePoints;
@property (nonatomic, strong) UIImage *colorImage;
@property (nonatomic, strong) UIImage *selectorImage;
@property (nonatomic, strong) UIView *container;

- (id)initWithFrame:(CGRect)frame colorImage:(UIImage *)colorImage;
- (void)setCirclePointPositions:(NSArray *)postions;

@end
