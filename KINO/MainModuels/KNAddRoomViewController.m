//
//  KNAddRoomViewController.m
//  KINO
//
//  Created by Aidian.Tang on 13-12-8.
//  Copyright (c) 2013年 Sunricher. All rights reserved.
//

#import "KNAddRoomViewController.h"
#import "GKImagePicker.h"
#import "UIImage+fixOrientation.h"

@interface KNAddRoomViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, GKImagePickerDelegate, GKImageCropControllerDelegate> {
    
}

@property (nonatomic, strong) IBOutlet UIView *cameraOverlayView;
@property (nonatomic, strong) IBOutlet UIButton *flashBtn;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, strong) GKImagePicker *imagePicker;

@end

@implementation KNAddRoomViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotiRoomDidSaved object:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //[self performSelector:@selector(initImagePickerController) withObject:nil afterDelay:1.];
    [self initImagePickerController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(roomDidSaved:) name:kNotiRoomDidSaved object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSNotificationCenter

- (void)roomDidSaved:(id)sender
{
    [_imagePickerController dismissViewControllerAnimated:YES completion:nil];
    [self.imagePicker.imagePickerController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Action

- (IBAction)back:(id)sender
{
    [_imagePickerController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)showGallery:(id)sender
{
    [_imagePickerController dismissViewControllerAnimated:YES completion:nil];
    
    self.imagePicker = [[GKImagePicker alloc] init];
    self.imagePicker.cropSize = CGSizeMake(296, 300);
    self.imagePicker.delegate = self;
	//self.imagePicker.resizeableCropArea = YES;
    
    [self presentViewController:self.imagePicker.imagePickerController animated:YES completion:nil];
}

- (IBAction)flash:(UIButton *)btn
{
    btn.selected = !btn.selected;
    if (btn.selected) {
        _imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
    } else {
        _imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOn;
    }
}

- (IBAction)takePicture:(id)sender
{
    [_imagePickerController takePicture];
}

#pragma mark - Private

- (void)initImagePickerController
{
    if (!_imagePickerController) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        } else {
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        if ([UIImagePickerController isFlashAvailableForCameraDevice:UIImagePickerControllerCameraDeviceRear]) {
            imagePickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOn;
        } else {
            _flashBtn.hidden = YES;
        }
        imagePickerController.showsCameraControls = NO;
        imagePickerController.cameraOverlayView = _cameraOverlayView;
        self.imagePickerController = imagePickerController;
    }
    [self presentViewController:_imagePickerController animated:NO completion:^{
        
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
//    UIImage *image = [[info objectForKey:UIImagePickerControllerEditedImage] imageByScalingAndCroppingForSize:CGSizeMake(640, 640)];
//    if (image) {
//        _selectedRoom.thumbnails = UIImagePNGRepresentation(image);
//        WFAppDelegate *delegate = (WFAppDelegate *)[[UIApplication sharedApplication] delegate];
//        [delegate saveContext];
//        [_tableView reloadData];
//    }
    
    GKImageCropViewController *cropController = [[GKImageCropViewController alloc] init];
    cropController.preferredContentSize = picker.preferredContentSize;
    cropController.sourceImage = [[info objectForKey:UIImagePickerControllerOriginalImage] fixOrientation];
    cropController.cropSize = CGSizeMake(296, 300);
    cropController.resizeableCropArea = YES;
    cropController.delegate = self;
    [picker pushViewController:cropController animated:YES];
    self.cropController = cropController;
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}

#pragma mark - GKImagePickerDelegate

- (void)imagePicker:(GKImagePicker *)imagePicker pickedImage:(UIImage *)image
{
    self.cropController = imagePicker.cropController;
    if (_delegate && [_delegate respondsToSelector:@selector(addRoomViewController:didFinishWithImage:)]) {
        [_delegate addRoomViewController:self didFinishWithImage:image];
    }
}

- (void)imagePickerDidCancel:(GKImagePicker *)imagePicker
{
    [imagePicker.imagePickerController dismissViewControllerAnimated:YES completion:^{
        [self initImagePickerController];
    }];
    
}

#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//     if ([segue.identifier isEqualToString:@"toKNEditRoomViewController"]) {
//          KNEditRoomViewController *editRoomViewController = segue.destinationViewController;
//         editRoomViewController.delegate = self;
//     }
}

#pragma mark - GKImageCropControllerDelegate

- (void)imageCropController:(GKImageCropViewController *)imageCropController didFinishWithCroppedImage:(UIImage *)croppedImage
{
    self.cropController = imageCropController;
    if (_delegate && [_delegate respondsToSelector:@selector(addRoomViewController:didFinishWithImage:)]) {
        [_delegate addRoomViewController:self didFinishWithImage:croppedImage];
    }
    NSLog(@"croppedImage = %@", NSStringFromCGSize(croppedImage.size));
}

@end
